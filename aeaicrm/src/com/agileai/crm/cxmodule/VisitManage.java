package com.agileai.crm.cxmodule;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.StandardService;

public interface VisitManage
        extends StandardService {
	
	public void updateFillInfoRecord(DataParam param);
	public void updateConfirmStateInfoRecord(DataParam param);
	public void updateVisitStateRecord(DataParam param);
	void createClueRecord(DataParam param);
	void createContRecord(DataParam param);
	public DataRow getProRecord(DataParam param);
}
