package com.agileai.crm.common;

import java.util.List;

import com.agileai.hotweb.domain.core.Role;
import com.agileai.hotweb.domain.core.User;

public class PrivilegeHelper {
	private User user = null;
	public PrivilegeHelper(User user){
		this.user = user;
}
	public boolean isInfoCollector(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("INFO_COLLECTOR".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	public boolean isTelNetSalesman(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("TEL_NET_SALESMAN".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	public boolean isSalesMan(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("SalesMan".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	public boolean isPreSale(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("PRE_SALE".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
	public boolean isSalesDirector(){
		boolean result = false;
		List<Role> roleList = user.getRoleList();
		for (int i=0;i < roleList.size();i++){
			Role role = roleList.get(i);
			if ("SalesMaster".equals(role.getRoleCode())){
				result = true;
				break;
			}
		}
		return result;
	}
}