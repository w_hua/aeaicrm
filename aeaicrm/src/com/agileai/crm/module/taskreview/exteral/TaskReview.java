package com.agileai.crm.module.taskreview.exteral;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/rest")
public interface TaskReview {
	
    @GET  
    @Path("/list/")
    @Produces(MediaType.TEXT_PLAIN)
	public String findWorkCheckList();
    
    @GET  
    @Path("/strange-list/{tcId}/{saleId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findStrangeVisitList(@PathParam("tcId") String tcId, @PathParam("saleId") String saleId);
    
    @GET  
    @Path("/intention-list/{tcId}/{saleId}")
    @Produces(MediaType.TEXT_PLAIN)
	public String findIntentionFollowUpList(@PathParam("tcId") String tcId, @PathParam("saleId") String saleId);
    
    @GET
    @Path("/get-summary-info/{tcId}/{saleId}") 
    @Produces(MediaType.TEXT_PLAIN)
	public String getSummaryInfo(@PathParam("tcId") String tcId, @PathParam("saleId") String saleId);
    
    @GET
    @Path("/update-summary-state/{taskReviewId}/{taskReviewState}") 
    @Produces(MediaType.TEXT_PLAIN)
	public String updateSummaryRecord(@PathParam("taskReviewId") String taskReviewId, @PathParam("taskReviewState") String taskReviewState);
    
}
