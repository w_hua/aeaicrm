/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : aeaicrm

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2018-03-13 14:53:27
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for crm_city
-- ----------------------------
DROP TABLE IF EXISTS `crm_city`;
CREATE TABLE `crm_city` (
  `CODE_ID` varchar(32) DEFAULT NULL,
  `CITY_ID` varchar(32) NOT NULL,
  `CITY_NAME` varchar(32) DEFAULT NULL,
  `CITY_SORT` int(32) DEFAULT NULL,
  PRIMARY KEY (`CITY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_city
-- ----------------------------
INSERT INTO `crm_city` VALUES ('SiChuan', 'ABa', '阿坝', '19');
INSERT INTO `crm_city` VALUES ('XinJiang', 'AKeSu', '阿克苏', '6');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'ALaShan', '阿拉善', '12');
INSERT INTO `crm_city` VALUES ('XinJiang', 'ALeTai', '阿勒泰', '14');
INSERT INTO `crm_city` VALUES ('XiZang', 'ALi', '阿里', '6');
INSERT INTO `crm_city` VALUES ('SanXi', 'AnKang', '安康', '9');
INSERT INTO `crm_city` VALUES ('AnHui', 'AnQing', '安庆', '8');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'AnShan', '鞍山', '3');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'AnShun', '安顺', '4');
INSERT INTO `crm_city` VALUES ('HeNan', 'AnYang', '安阳', '8');
INSERT INTO `crm_city` VALUES ('JiLin', 'BaiCheng', '白城', '8');
INSERT INTO `crm_city` VALUES ('GuangXi', 'BaiSe', '百色', '10');
INSERT INTO `crm_city` VALUES ('JiLin', 'BaiShan', '白山', '6');
INSERT INTO `crm_city` VALUES ('GanSu', 'BaiYin', '白银', '4');
INSERT INTO `crm_city` VALUES ('HeBei', 'BaoDing', '保定', '6');
INSERT INTO `crm_city` VALUES ('SanXi', 'BaoJi', '宝鸡', '3');
INSERT INTO `crm_city` VALUES ('YunNan', 'BaoShan', '保山', '4');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'BaoTou', '包头', '2');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'BaYanNaoEr', '巴彦淖尔', '8');
INSERT INTO `crm_city` VALUES ('XinJiang', 'BaYinGuoLeMengGu', '巴音郭楞蒙古', '9');
INSERT INTO `crm_city` VALUES ('SiChuan', 'BaZhong', '巴中', '17');
INSERT INTO `crm_city` VALUES ('GuangXi', 'BeiHai', '北海', '5');
INSERT INTO `crm_city` VALUES ('AnHui', 'BengBu', '蚌埠', '3');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'BenXi', '本溪', '5');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'BiJie', '毕节', '6');
INSERT INTO `crm_city` VALUES ('ShanDong', 'BinZhou', '滨州', '16');
INSERT INTO `crm_city` VALUES ('XinJiang', 'BoErTaLaMengGu', '博尔塔拉蒙古', '11');
INSERT INTO `crm_city` VALUES ('AnHui', 'BoZhou', '亳州', '15');
INSERT INTO `crm_city` VALUES ('HeBei', 'CangZhou', '沧州', '9');
INSERT INTO `crm_city` VALUES ('JiLin', 'ChangChun', '长春', '1');
INSERT INTO `crm_city` VALUES ('HuNan', 'ChangDe', '常德', '7');
INSERT INTO `crm_city` VALUES ('XiZang', 'ChangDu', '昌都', '2');
INSERT INTO `crm_city` VALUES ('XinJiang', 'ChangJi', '昌吉', '10');
INSERT INTO `crm_city` VALUES ('HuNan', 'ChangSha', '长沙', '1');
INSERT INTO `crm_city` VALUES ('Shanxi', 'ChangZhi', '长治', '4');
INSERT INTO `crm_city` VALUES ('JiangSu', 'ChangZhou', '常州', '4');
INSERT INTO `crm_city` VALUES ('AnHui', 'ChaoHu', '巢湖', '13');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'ChaoYang', '朝阳', '13');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ChaoZhou', '潮州', '19');
INSERT INTO `crm_city` VALUES ('HeBei', 'ChengDe', '承德', '8');
INSERT INTO `crm_city` VALUES ('SiChuan', 'ChengDu', '成都', '1');
INSERT INTO `crm_city` VALUES ('HuNan', 'ChenZhou', '郴州', '10');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'ChiFeng', '赤峰', '4');
INSERT INTO `crm_city` VALUES ('AnHui', 'ChiZhou', '池州', '16');
INSERT INTO `crm_city` VALUES ('GuangXi', 'ChongZuo', '崇左', '14');
INSERT INTO `crm_city` VALUES ('YunNan', 'ChuXiong', '楚雄', '12');
INSERT INTO `crm_city` VALUES ('AnHui', 'ChuZhou', '滁州', '10');
INSERT INTO `crm_city` VALUES ('YunNan', 'DaLi', '大理', '13');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'DaLian', '大连', '2');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'DanDong', '丹东', '6');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'DaQing', '大庆', '6');
INSERT INTO `crm_city` VALUES ('Shanxi', 'DaTong', '大同', '2');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'DaXingAnLing', '大兴安岭', '13');
INSERT INTO `crm_city` VALUES ('SiChuan', 'DaZhou', '达州', '14');
INSERT INTO `crm_city` VALUES ('YunNan', 'DeHong', '德宏', '14');
INSERT INTO `crm_city` VALUES ('SiChuan', 'DeYang', '德阳', '5');
INSERT INTO `crm_city` VALUES ('ShanDong', 'DeZhou', '德州', '14');
INSERT INTO `crm_city` VALUES ('GanSu', 'DingXi', '定西', '11');
INSERT INTO `crm_city` VALUES ('YunNan', 'DiQing', '迪庆', '16');
INSERT INTO `crm_city` VALUES ('GuangDong', 'DongGuan', '东莞', '17');
INSERT INTO `crm_city` VALUES ('ShanDong', 'DongYing', '东营', '5');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'EErDuoSi', '鄂尔多斯', '6');
INSERT INTO `crm_city` VALUES ('HuBei', 'EnShi', '恩施', '13');
INSERT INTO `crm_city` VALUES ('HuBei', 'EZhou', '鄂州', '8');
INSERT INTO `crm_city` VALUES ('GuangXi', 'FangChengGang', '防城港', '6');
INSERT INTO `crm_city` VALUES ('GuangDong', 'Foshan', '佛山', '6');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'FuShun', '抚顺', '4');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'FuXin', '阜新', '9');
INSERT INTO `crm_city` VALUES ('AnHui', 'FuYang', '阜阳', '11');
INSERT INTO `crm_city` VALUES ('FuJian', 'FuZhou', '福州', '1');
INSERT INTO `crm_city` VALUES ('JiangXi', 'FuZhouShi', '抚州', '10');
INSERT INTO `crm_city` VALUES ('GanSu', 'GanNan', '甘南', '14');
INSERT INTO `crm_city` VALUES ('JiangXi', 'GanZhou', '赣州', '7');
INSERT INTO `crm_city` VALUES ('SiChuan', 'GanZi', '甘孜', '20');
INSERT INTO `crm_city` VALUES ('TaiWan', 'GaoXiong', '高雄', '2');
INSERT INTO `crm_city` VALUES ('SiChuan', 'GuangAn', '广安', '13');
INSERT INTO `crm_city` VALUES ('SiChuan', 'GuangYuan', '广元', '7');
INSERT INTO `crm_city` VALUES ('GuangDong', 'GuangZhou', '广州', '1');
INSERT INTO `crm_city` VALUES ('GuangXi', 'GuiGang', '贵港', '8');
INSERT INTO `crm_city` VALUES ('GuangXi', 'GuiLin', '桂林', '3');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'GuiYang', '贵阳', '1');
INSERT INTO `crm_city` VALUES ('QingHai', 'GuoLuo', '果洛', '6');
INSERT INTO `crm_city` VALUES ('NingXia', 'GuYuan', '固原', '4');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'HaErBin', '哈尔滨', '1');
INSERT INTO `crm_city` VALUES ('QingHai', 'HaiBei', '海北', '3');
INSERT INTO `crm_city` VALUES ('QingHai', 'HaiDong', '海东', '2');
INSERT INTO `crm_city` VALUES ('HaiNan', 'HaiKou', '海口', '1');
INSERT INTO `crm_city` VALUES ('QingHai', 'HaiNanShi', '海南', '5');
INSERT INTO `crm_city` VALUES ('QingHai', 'HaiXi', '海西', '8');
INSERT INTO `crm_city` VALUES ('XinJiang', 'HaMi', '哈密', '4');
INSERT INTO `crm_city` VALUES ('HeBei', 'HanDan', '邯郸', '4');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'HangZhou', '杭州', '1');
INSERT INTO `crm_city` VALUES ('SanXi', 'HanZhong', '汉中', '7');
INSERT INTO `crm_city` VALUES ('HeNan', 'HeBi', '鹤壁', '6');
INSERT INTO `crm_city` VALUES ('GuangXi', 'HeChi', '河池', '12');
INSERT INTO `crm_city` VALUES ('AnHui', 'HeFei', '合肥', '1');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'HeGang', '鹤岗', '4');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'HeiHe', '黑河', '11');
INSERT INTO `crm_city` VALUES ('HeBei', 'HengShui', '衡水', '11');
INSERT INTO `crm_city` VALUES ('HuNan', 'HengYang', '衡阳', '4');
INSERT INTO `crm_city` VALUES ('XinJiang', 'HeTian', '和田', '5');
INSERT INTO `crm_city` VALUES ('GuangDong', 'HeYuan', '河源', '14');
INSERT INTO `crm_city` VALUES ('ShanDong', 'HeZe', '菏泽', '17');
INSERT INTO `crm_city` VALUES ('GuangXi', 'HeZhou', '贺州', '11');
INSERT INTO `crm_city` VALUES ('YunNan', 'HongHe', '红河', '10');
INSERT INTO `crm_city` VALUES ('JiangSu', 'HuaiAn', '淮安', '8');
INSERT INTO `crm_city` VALUES ('AnHui', 'HuaiBei', '淮北', '6');
INSERT INTO `crm_city` VALUES ('HuNan', 'HuaiHua', '怀化', '12');
INSERT INTO `crm_city` VALUES ('AnHui', 'HuaiNan', '淮南', '4');
INSERT INTO `crm_city` VALUES ('HuBei', 'HuangGang', '黄冈', '10');
INSERT INTO `crm_city` VALUES ('QingHai', 'HuangNan', '黄南', '4');
INSERT INTO `crm_city` VALUES ('AnHui', 'HuangShan', '黄山', '9');
INSERT INTO `crm_city` VALUES ('HuBei', 'Huangshi', '黄石', '2');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'HuHuHaoTe', '呼呼浩特', '1');
INSERT INTO `crm_city` VALUES ('GuangDong', 'HuiZhou', '惠州', '11');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'HuLuDao', '葫芦岛', '14');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'HuLuoBeiEr', '呼伦贝尔', '7');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'HuZhou', '湖州', '5');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'JiaMuSi', '佳木斯', '8');
INSERT INTO `crm_city` VALUES ('JiangXi', 'JiAn', '吉安', '8');
INSERT INTO `crm_city` VALUES ('GuangDong', 'JiangMen', '江门', '7');
INSERT INTO `crm_city` VALUES ('HeNan', 'JiaoZuo', '焦作', '5');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'JiaXing', '嘉兴', '4');
INSERT INTO `crm_city` VALUES ('TaiWan', 'JiaYi', '嘉义', '7');
INSERT INTO `crm_city` VALUES ('GanSu', 'JiaYuGuan', '嘉峪关', '2');
INSERT INTO `crm_city` VALUES ('GuangDong', 'JieYang', '揭阳', '20');
INSERT INTO `crm_city` VALUES ('JiLin', 'JiLinShi', '吉林', '2');
INSERT INTO `crm_city` VALUES ('TaiWan', 'JiLong', '基隆', '3');
INSERT INTO `crm_city` VALUES ('ShanDong', 'JiNan', '济南', '1');
INSERT INTO `crm_city` VALUES ('GanSu', 'JinChang', '金昌', '3');
INSERT INTO `crm_city` VALUES ('Shanxi', 'JinCheng', '晋城', '5');
INSERT INTO `crm_city` VALUES ('JiangXi', 'JingDeZhen', '景德镇', '2');
INSERT INTO `crm_city` VALUES ('HuBei', 'JingMen', '荆门', '7');
INSERT INTO `crm_city` VALUES ('HuBei', 'JingZhou', '荆州', '5');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'JinHua', '金华', '7');
INSERT INTO `crm_city` VALUES ('ShanDong', 'JiNing', '济宁', '9');
INSERT INTO `crm_city` VALUES ('Shanxi', 'JinZhong', '晋中', '7');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'JinZhou', '锦州', '7');
INSERT INTO `crm_city` VALUES ('JiangXi', 'JiuJiang', '九江', '4');
INSERT INTO `crm_city` VALUES ('GanSu', 'JiuQuan', '酒泉', '9');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'JiXi', '鸡西', '3');
INSERT INTO `crm_city` VALUES ('HeNan', 'KaiFeng', '开封', '2');
INSERT INTO `crm_city` VALUES ('XinJiang', 'KaShi', '喀什', '7');
INSERT INTO `crm_city` VALUES ('XinJiang', 'KeLaMaYi', '克拉玛依', '2');
INSERT INTO `crm_city` VALUES ('XinJiang', 'KeZiLeSuKeErKeZi', '克孜勒苏柯尔克孜', '8');
INSERT INTO `crm_city` VALUES ('YunNan', 'KunMing', '昆明', '1');
INSERT INTO `crm_city` VALUES ('GuangXi', 'LaiBin', '来宾', '13');
INSERT INTO `crm_city` VALUES ('ShanDong', 'LaiWu', '莱芜', '12');
INSERT INTO `crm_city` VALUES ('HeBei', 'LangFang', '廊坊', '10');
INSERT INTO `crm_city` VALUES ('GanSu', 'LanZhou', '兰州', '1');
INSERT INTO `crm_city` VALUES ('XiZang', 'LaSa', '拉萨', '1');
INSERT INTO `crm_city` VALUES ('SiChuan', 'LeShan', '乐山', '10');
INSERT INTO `crm_city` VALUES ('SiChuan', 'LiangShan', '凉山', '21');
INSERT INTO `crm_city` VALUES ('JiangSu', 'LianYunGang', '连云港', '7');
INSERT INTO `crm_city` VALUES ('ShanDong', 'LiaoCheng', '聊城', '15');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'LiaoYang', '辽阳', '10');
INSERT INTO `crm_city` VALUES ('JiLin', 'LiaoYuan', '辽源', '4');
INSERT INTO `crm_city` VALUES ('YunNan', 'LiJiang', '丽江', '6');
INSERT INTO `crm_city` VALUES ('YunNan', 'LinCang', '临沧', '8');
INSERT INTO `crm_city` VALUES ('Shanxi', 'LinFen', '临汾', '10');
INSERT INTO `crm_city` VALUES ('GanSu', 'LinXia', '临夏', '13');
INSERT INTO `crm_city` VALUES ('ShanDong', 'LinYi', '临沂', '13');
INSERT INTO `crm_city` VALUES ('XiZang', 'LinZhi', '林芝', '7');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'LiShui', '丽水', '11');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'LiuPanShui', '六盘水', '2');
INSERT INTO `crm_city` VALUES ('GuangXi', 'LiuZhou', '柳州', '2');
INSERT INTO `crm_city` VALUES ('GanSu', 'LongNan', '陇南', '12');
INSERT INTO `crm_city` VALUES ('FuJian', 'LongYan', '龙岩', '8');
INSERT INTO `crm_city` VALUES ('HuNan', 'LouDi', '娄底', '13');
INSERT INTO `crm_city` VALUES ('AnHui', 'LuAn', '六安', '14');
INSERT INTO `crm_city` VALUES ('HeNan', 'LuoHe', '漯河', '11');
INSERT INTO `crm_city` VALUES ('HeNan', 'Luoyang', '洛阳', '3');
INSERT INTO `crm_city` VALUES ('SiChuan', 'LuZhou', '泸州', '4');
INSERT INTO `crm_city` VALUES ('Shanxi', 'LvLiang', '吕梁', '11');
INSERT INTO `crm_city` VALUES ('AnHui', 'MaAnShan', '马鞍山', '5');
INSERT INTO `crm_city` VALUES ('GuangDong', 'MaoMing', '茂名', '9');
INSERT INTO `crm_city` VALUES ('SiChuan', 'MeiShan', '眉山', '15');
INSERT INTO `crm_city` VALUES ('GuangDong', 'MeiZhou', '梅州', '12');
INSERT INTO `crm_city` VALUES ('SiChuan', 'MianYang', '绵阳', '6');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'MuDanJiang', '牡丹江', '10');
INSERT INTO `crm_city` VALUES ('JiangXi', 'NanChang', '南昌', '1');
INSERT INTO `crm_city` VALUES ('SiChuan', 'NanChong', '南充', '11');
INSERT INTO `crm_city` VALUES ('JiangSu', 'NanJing', '南京', '1');
INSERT INTO `crm_city` VALUES ('GuangXi', 'NanNing', '南宁', '1');
INSERT INTO `crm_city` VALUES ('FuJian', 'NanPing', '南平', '7');
INSERT INTO `crm_city` VALUES ('JiangSu', 'NanTong', '南通', '6');
INSERT INTO `crm_city` VALUES ('HeNan', 'NanYang', '南阳', '13');
INSERT INTO `crm_city` VALUES ('XiZang', 'NaQv', '那曲', '5');
INSERT INTO `crm_city` VALUES ('SiChuan', 'NeiJiang', '内江', '9');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'NingBo', '宁波', '2');
INSERT INTO `crm_city` VALUES ('FuJian', 'NingDe', '宁德', '9');
INSERT INTO `crm_city` VALUES ('YunNan', 'NuJiang', '怒江', '15');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'PanJin', '盘锦', '11');
INSERT INTO `crm_city` VALUES ('SiChuan', 'PanZhiHua', '攀枝花', '3');
INSERT INTO `crm_city` VALUES ('HeNan', 'PingDingShan', '平顶山', '4');
INSERT INTO `crm_city` VALUES ('GanSu', 'PingLiang', '平凉', '8');
INSERT INTO `crm_city` VALUES ('JiangXi', 'PingXiang', '萍乡', '3');
INSERT INTO `crm_city` VALUES ('YunNan', 'PuEr', '普洱', '7');
INSERT INTO `crm_city` VALUES ('FuJian', 'PuTian', '莆田', '3');
INSERT INTO `crm_city` VALUES ('HeNan', 'PuYang', '濮阳', '9');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'QianDongNan', '黔东南', '8');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'QianNan', '黔南', '9');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'QianXiNan', '黔西南', '7');
INSERT INTO `crm_city` VALUES ('ShanDong', 'QingDao', '青岛', '2');
INSERT INTO `crm_city` VALUES ('HeBei', 'QingHuangDao', '秦皇岛', '3');
INSERT INTO `crm_city` VALUES ('GanSu', 'QingYang', '庆阳', '10');
INSERT INTO `crm_city` VALUES ('GuangDong', 'QingYuan', '清远', '16');
INSERT INTO `crm_city` VALUES ('GuangXi', 'QinZhou', '钦州', '7');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'QiQiHaEr', '齐齐哈尔', '2');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'QiTaiHe', '七台河', '9');
INSERT INTO `crm_city` VALUES ('FuJian', 'QuanZhou', '泉州', '5');
INSERT INTO `crm_city` VALUES ('YunNan', 'QvJing', '曲靖', '2');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'QvZhou', '衢州', '8');
INSERT INTO `crm_city` VALUES ('XiZang', 'RiKaZe', '日喀则', '4');
INSERT INTO `crm_city` VALUES ('ShanDong', 'RiZhao', '日照', '11');
INSERT INTO `crm_city` VALUES ('HeNan', 'SanMenXia', '三门峡', '12');
INSERT INTO `crm_city` VALUES ('FuJian', 'SanMing', '三明', '4');
INSERT INTO `crm_city` VALUES ('HaiNan', 'SanYa', '三亚', '2');
INSERT INTO `crm_city` VALUES ('SanXi', 'ShangLuo', '商洛', '10');
INSERT INTO `crm_city` VALUES ('HeNan', 'ShangQiU', '商丘', '14');
INSERT INTO `crm_city` VALUES ('JiangXi', 'ShangRao', '上饶', '11');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'ShangYaShan', '双鸭山', '5');
INSERT INTO `crm_city` VALUES ('XiZang', 'ShanNan', '山南', '3');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ShanTou', '汕头', '4');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ShanWei', '汕尾', '13');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ShaoGuan', '韶关', '5');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'ShaoXing', '绍兴', '6');
INSERT INTO `crm_city` VALUES ('HuNan', 'ShaoYang', '邵阳', '5');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'ShenYang', '沈阳', '1');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ShenZhen', '深圳', '2');
INSERT INTO `crm_city` VALUES ('HeBei', 'ShiJiaZhuag', '石家庄', '1');
INSERT INTO `crm_city` VALUES ('HuBei', 'ShiYan', '十堰', '4');
INSERT INTO `crm_city` VALUES ('NingXia', 'ShiZuiShan', '石嘴山', '2');
INSERT INTO `crm_city` VALUES ('JiLin', 'SiPing', '四平', '3');
INSERT INTO `crm_city` VALUES ('JiLin', 'SongYuan', '松原', '7');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'SuiHua', '绥化', '12');
INSERT INTO `crm_city` VALUES ('SiChuan', 'SuiNing', '遂宁', '8');
INSERT INTO `crm_city` VALUES ('HuBei', 'SuiZhou', '随州', '12');
INSERT INTO `crm_city` VALUES ('Shanxi', 'SuoZhou', '朔州', '6');
INSERT INTO `crm_city` VALUES ('JiangSu', 'SuQian', '宿迁', '13');
INSERT INTO `crm_city` VALUES ('JiangSu', 'SuZhou', '苏州', '5');
INSERT INTO `crm_city` VALUES ('AnHui', 'SuZhouShi', '宿州', '12');
INSERT INTO `crm_city` VALUES ('XinJiang', 'TaCheng', '塔城', '13');
INSERT INTO `crm_city` VALUES ('ShanDong', 'TaiAn', '泰安', '10');
INSERT INTO `crm_city` VALUES ('TaiWan', 'TaiBei', '台北', '1');
INSERT INTO `crm_city` VALUES ('TaiWan', 'TaiNan', '台南', '5');
INSERT INTO `crm_city` VALUES ('Shanxi', 'TaiYuan', '太原', '1');
INSERT INTO `crm_city` VALUES ('TaiWan', 'TaiZhong', '台中', '4');
INSERT INTO `crm_city` VALUES ('JiangSu', 'TaiZhou', '泰州', '12');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'TaiZhouShi', '台州', '10');
INSERT INTO `crm_city` VALUES ('HeBei', 'TangShan', '唐山', '2');
INSERT INTO `crm_city` VALUES ('GanSu', 'TianShui', '天水', '5');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'TieLing', '铁岭', '12');
INSERT INTO `crm_city` VALUES ('SanXi', 'TongChuan', '铜川', '2');
INSERT INTO `crm_city` VALUES ('JiLin', 'TongHua', '通化', '5');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'TongLiao', '通辽', '5');
INSERT INTO `crm_city` VALUES ('AnHui', 'TongLing', '铜陵', '7');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'TongRen', '铜仁', '5');
INSERT INTO `crm_city` VALUES ('XinJiang', 'TuLuFan', '吐鲁番', '3');
INSERT INTO `crm_city` VALUES ('ShanDong', 'WeiFang', '潍坊', '7');
INSERT INTO `crm_city` VALUES ('ShanDong', 'WeiHai', '威海', '8');
INSERT INTO `crm_city` VALUES ('SanXi', 'WeiNan', '渭南', '5');
INSERT INTO `crm_city` VALUES ('YunNan', 'WenShan', '文山', '9');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'WenZhou', '温州', '3');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'WuHai', '乌海', '3');
INSERT INTO `crm_city` VALUES ('HuBei', 'WuHan', '武汉', '1');
INSERT INTO `crm_city` VALUES ('AnHui', 'WuHu', '芜湖', '2');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'WuLanChaBu', '乌兰察布', '9');
INSERT INTO `crm_city` VALUES ('XinJiang', 'WuLuMuQi', '乌鲁木齐', '1');
INSERT INTO `crm_city` VALUES ('GanSu', 'WuWei', '武威', '6');
INSERT INTO `crm_city` VALUES ('JiangSu', 'WuXi', '无锡', '2');
INSERT INTO `crm_city` VALUES ('NingXia', 'WuZhong', '吴忠', '3');
INSERT INTO `crm_city` VALUES ('GuangXi', 'WuZhou', '梧州', '4');
INSERT INTO `crm_city` VALUES ('FuJian', 'XiaMen', '厦门', '2');
INSERT INTO `crm_city` VALUES ('SanXi', 'XiAn', '西安', '1');
INSERT INTO `crm_city` VALUES ('HuBei', 'XiangFan', '襄樊', '3');
INSERT INTO `crm_city` VALUES ('HuNan', 'XiangTan', '湘潭', '3');
INSERT INTO `crm_city` VALUES ('HuNan', 'XiangXi', '湘西', '14');
INSERT INTO `crm_city` VALUES ('HuBei', 'XianNing', '咸宁', '11');
INSERT INTO `crm_city` VALUES ('SanXi', 'XianYang', '咸阳', '4');
INSERT INTO `crm_city` VALUES ('HuBei', 'XiaoGan', '孝感', '9');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'XiLinGuoLe', '锡林郭勒', '11');
INSERT INTO `crm_city` VALUES ('NeiMengGu', 'XingAn', '兴安', '10');
INSERT INTO `crm_city` VALUES ('HeBei', 'XingTan', '邢台', '5');
INSERT INTO `crm_city` VALUES ('QingHai', 'XiNing', '西宁', '1');
INSERT INTO `crm_city` VALUES ('HeNan', 'XinXiang', '新乡', '7');
INSERT INTO `crm_city` VALUES ('HeNan', 'XinYang', '信阳', '15');
INSERT INTO `crm_city` VALUES ('JiangXi', 'XinYu', '新余', '5');
INSERT INTO `crm_city` VALUES ('Shanxi', 'XinZhou', '忻州', '9');
INSERT INTO `crm_city` VALUES ('TaiWan', 'XinZhu', '新竹', '6');
INSERT INTO `crm_city` VALUES ('YunNan', 'XiShungBanNa', '西双版纳', '11');
INSERT INTO `crm_city` VALUES ('AnHui', 'XuanCheng', '宣城', '17');
INSERT INTO `crm_city` VALUES ('HeNan', 'XvChang', '许昌', '10');
INSERT INTO `crm_city` VALUES ('JiangSu', 'XvZhou', '徐州', '3');
INSERT INTO `crm_city` VALUES ('SiChuan', 'YaAn', '雅安', '16');
INSERT INTO `crm_city` VALUES ('SanXi', 'YanAn', '延安', '6');
INSERT INTO `crm_city` VALUES ('JiLin', 'YanBian', '延边', '9');
INSERT INTO `crm_city` VALUES ('JiangSu', 'YanCheng', '盐城', '9');
INSERT INTO `crm_city` VALUES ('GuangDong', 'YangJiang', '阳江', '15');
INSERT INTO `crm_city` VALUES ('Shanxi', 'YangQuan', '阳泉', '3');
INSERT INTO `crm_city` VALUES ('JiangSu', 'YangZhou', '扬州', '10');
INSERT INTO `crm_city` VALUES ('ShanDong', 'YanTai', '烟台', '6');
INSERT INTO `crm_city` VALUES ('SiChuan', 'YiBin', '宜宾', '12');
INSERT INTO `crm_city` VALUES ('HuBei', 'YiChang', '宜昌', '6');
INSERT INTO `crm_city` VALUES ('HeiLongJiang', 'YiChun', '伊春', '7');
INSERT INTO `crm_city` VALUES ('JiangXi', 'YiChunShi', '宜春', '9');
INSERT INTO `crm_city` VALUES ('XinJiang', 'YiLiHaSaKe', '伊犁哈萨克', '12');
INSERT INTO `crm_city` VALUES ('NingXia', 'YinChuan', ' 银川', '1');
INSERT INTO `crm_city` VALUES ('LiaoNing', 'YingKou', '营口', '8');
INSERT INTO `crm_city` VALUES ('JiangXi', 'YingTan', '鹰潭', '6');
INSERT INTO `crm_city` VALUES ('HuNan', 'YiYang', '益阳', '9');
INSERT INTO `crm_city` VALUES ('HuNan', 'YongZhou', '永州', '11');
INSERT INTO `crm_city` VALUES ('HuNan', 'YueYang', '岳阳', '6');
INSERT INTO `crm_city` VALUES ('GuangXi', 'YuLin', '玉林', '9');
INSERT INTO `crm_city` VALUES ('SanXi', 'YuLinShi', '榆林', '8');
INSERT INTO `crm_city` VALUES ('Shanxi', 'YunCheng', '运城', '8');
INSERT INTO `crm_city` VALUES ('GuangDong', 'YunFu', '云浮', '21');
INSERT INTO `crm_city` VALUES ('QingHai', 'YuShu', '玉树', '7');
INSERT INTO `crm_city` VALUES ('YunNan', 'YuXi', '玉溪', '3');
INSERT INTO `crm_city` VALUES ('ShanDong', 'ZaoZhuang', '枣庄', '4');
INSERT INTO `crm_city` VALUES ('HuNan', 'ZhangJiaJie', '张家界', '8');
INSERT INTO `crm_city` VALUES ('HeBei', 'ZhangJiaKou', '张家口', '7');
INSERT INTO `crm_city` VALUES ('GanSu', 'ZhangYe', '张掖', '7');
INSERT INTO `crm_city` VALUES ('FuJian', 'ZhangZhou', '漳州', '6');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ZhanJiang', '湛江', '8');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ZhaoQing', '肇庆', '10');
INSERT INTO `crm_city` VALUES ('YunNan', 'ZhaoTong', '昭通', '5');
INSERT INTO `crm_city` VALUES ('HeNan', 'ZhengZhou', '郑州', '1');
INSERT INTO `crm_city` VALUES ('JiangSu', 'ZhenJiang', '镇江', '11');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ZhongShan', '中山', '18');
INSERT INTO `crm_city` VALUES ('NingXia', 'ZhongWei', '中卫', '5');
INSERT INTO `crm_city` VALUES ('HeNan', 'ZhouKou', '周口', '16');
INSERT INTO `crm_city` VALUES ('ZheJiang', 'ZhouShan', '舟山', '9');
INSERT INTO `crm_city` VALUES ('GuangDong', 'ZhuHai', '珠海', '3');
INSERT INTO `crm_city` VALUES ('HeNan', 'ZhuMaDian', '驻马店', '17');
INSERT INTO `crm_city` VALUES ('HuNan', 'ZhuZhou', '株洲', '2');
INSERT INTO `crm_city` VALUES ('ShanDong', 'ZiBo', '淄博', '3');
INSERT INTO `crm_city` VALUES ('SiChuan', 'ZiGong', '自贡', '2');
INSERT INTO `crm_city` VALUES ('SiChuan', 'ZiYang', '资阳', '18');
INSERT INTO `crm_city` VALUES ('GuiZhou', 'ZunYi', '遵义', '3');

-- ----------------------------
-- Table structure for crm_contact_person
-- ----------------------------
DROP TABLE IF EXISTS `crm_contact_person`;
CREATE TABLE `crm_contact_person` (
  `CONT_ID` char(36) NOT NULL,
  `CUST_ID` char(36) DEFAULT NULL,
  `CONT_JOB` varchar(32) DEFAULT NULL,
  `CONT_NAME` varchar(32) DEFAULT NULL,
  `CONT_SEX` varchar(32) DEFAULT NULL,
  `CONT_PHONE` varchar(32) DEFAULT NULL,
  `CONT_EMAIL` varchar(32) DEFAULT NULL,
  `CONT_OTHER` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`CONT_ID`),
  KEY `FK_Reference_3` (`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_contact_person
-- ----------------------------

-- ----------------------------
-- Table structure for crm_customer_group
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer_group`;
CREATE TABLE `crm_customer_group` (
  `GRP_ID` char(36) NOT NULL,
  `GRP_SUP_ID` char(36) DEFAULT NULL,
  `GRP_CODE` varchar(32) DEFAULT NULL,
  `GRP_NAME` varchar(32) DEFAULT NULL,
  `GRP_SORT` int(11) DEFAULT NULL,
  `GRP_STATE` varchar(32) DEFAULT NULL,
  `GRP_DESC` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_customer_group
-- ----------------------------
INSERT INTO `crm_customer_group` VALUES ('00000000-0000-0000-00000000000000000', '', 'Root', '组织名称', '0', '1', '');
INSERT INTO `crm_customer_group` VALUES ('2FCAAFDE-E2E8-41C2-9C1A-18248650F761', '00000000-0000-0000-00000000000000000', 'Government', '政务相关', '18', null, null);
INSERT INTO `crm_customer_group` VALUES ('338805DC-4095-4E00-B6DC-181F573D9090', '00000000-0000-0000-00000000000000000', 'HotelStores', '酒店卖场', '9', null, null);
INSERT INTO `crm_customer_group` VALUES ('5636C0A6-265D-4C25-A6C7-515A53577800', '00000000-0000-0000-00000000000000000', 'RetailIndustry', '零售行业', '10', null, null);
INSERT INTO `crm_customer_group` VALUES ('597E8622-944A-44A7-BE5A-28E95F7834D6', '00000000-0000-0000-00000000000000000', 'TEMP', '临时分组', '5', null, null);
INSERT INTO `crm_customer_group` VALUES ('6EDF0BBE-7CCC-4423-9E68-4AD0A47C9B36', '00000000-0000-0000-00000000000000000', 'Heavymanufacturing', '制造重工', '19', null, null);
INSERT INTO `crm_customer_group` VALUES ('7BE8ED1C-784F-42BA-8C98-18D0741C4782', '00000000-0000-0000-00000000000000000', 'BankTelecom', '银行电信', '17', null, null);
INSERT INTO `crm_customer_group` VALUES ('877A3C17-603A-4D2B-A9DC-2EEC82BEDB9A', '00000000-0000-0000-00000000000000000', 'RelatedCase', '相关案例', '13', null, null);
INSERT INTO `crm_customer_group` VALUES ('932E1980-8078-4C15-95A7-9C8E5F3D5CB6', '00000000-0000-0000-00000000000000000', 'Otherindustries', '其它行业', '12', null, null);
INSERT INTO `crm_customer_group` VALUES ('9AA7658E-31E5-42BB-860D-6B4EF70814CF', '00000000-0000-0000-00000000000000000', 'Pharmaceuticalindustry', '医药行业', '16', null, null);
INSERT INTO `crm_customer_group` VALUES ('BDB7F8CB-761D-47A6-A8B1-96222DA734C9', '00000000-0000-0000-00000000000000000', 'Transportation', '交通运输', '7', null, null);
INSERT INTO `crm_customer_group` VALUES ('D2133B90-3532-480C-9BC9-A9B9842C7375', '00000000-0000-0000-00000000000000000', 'EducationCampus', '教育校园', '8', null, null);
INSERT INTO `crm_customer_group` VALUES ('E18B9133-A846-4B38-A911-2C710BFADCD3', '00000000-0000-0000-00000000000000000', 'ConstructionProperty', '建筑地产', '6', null, null);
INSERT INTO `crm_customer_group` VALUES ('EFAE7889-AA26-48EC-A9FB-37C4FC10C88C', '00000000-0000-0000-00000000000000000', 'Healthcare', '医疗卫生', '15', null, null);
INSERT INTO `crm_customer_group` VALUES ('F3CEA72B-398A-42D2-A1D1-545867D768EF', '00000000-0000-0000-00000000000000000', 'CoalEnergy', '煤炭能源', '11', null, null);
INSERT INTO `crm_customer_group` VALUES ('FB30368E-E04C-4420-968C-F7E0A3A946B5', '00000000-0000-0000-00000000000000000', 'TobaccoIndustry', '烟草行业', '14', null, null);

-- ----------------------------
-- Table structure for crm_customer_grp_rel
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer_grp_rel`;
CREATE TABLE `crm_customer_grp_rel` (
  `GRP_ID` char(36) NOT NULL,
  `CUST_ID` char(36) NOT NULL,
  PRIMARY KEY (`GRP_ID`,`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_customer_grp_rel
-- ----------------------------

-- ----------------------------
-- Table structure for crm_customer_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer_info`;
CREATE TABLE `crm_customer_info` (
  `CUST_ID` char(36) NOT NULL,
  `CUST_NAME` varchar(32) DEFAULT NULL,
  `CUST_INDUSTRY` varchar(32) DEFAULT NULL,
  `CUST_PROVINCE` varchar(32) DEFAULT NULL,
  `CUST_CITY` varchar(32) DEFAULT NULL,
  `CUST_ADDRESS` varchar(128) DEFAULT NULL,
  `CUST_SCALE` varchar(32) DEFAULT NULL,
  `CUST_NATURE` varchar(32) DEFAULT NULL,
  `CUST_INTRODUCE` varchar(1024) DEFAULT NULL,
  `CUST_STATE` varchar(32) DEFAULT NULL,
  `CUST_CREATE_ID` char(36) DEFAULT NULL,
  `CUST_CREATE_TIME` datetime DEFAULT NULL,
  `CUST_SUBMIT_ID` char(36) DEFAULT NULL,
  `CUST_SUBMIT_TIME` datetime DEFAULT NULL,
  `CUST_CONFIRM_ID` char(36) DEFAULT NULL,
  `CUST_CONFIRM_TIME` datetime DEFAULT NULL,
  `CUST_PROGRESS_STATE` varchar(32) DEFAULT NULL,
  `CUST_LEVEL` varchar(32) DEFAULT NULL,
  `CUST_COMPANY_WEB` varchar(128) DEFAULT NULL,
  `ORG_ID` char(36) DEFAULT NULL,
  `CUST_VISIT_AGAIN_TIME` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_customer_info
-- ----------------------------

-- ----------------------------
-- Table structure for crm_customer_sales_rel
-- ----------------------------
DROP TABLE IF EXISTS `crm_customer_sales_rel`;
CREATE TABLE `crm_customer_sales_rel` (
  `CUST_ID` char(36) DEFAULT NULL,
  `USER_ID` char(36) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_customer_sales_rel
-- ----------------------------

-- ----------------------------
-- Table structure for crm_import_temp
-- ----------------------------
DROP TABLE IF EXISTS `crm_import_temp`;
CREATE TABLE `crm_import_temp` (
  `IMPORT_TEMP_ID` char(36) NOT NULL,
  `IMPORT_TEMP_NAME` varchar(32) DEFAULT NULL,
  `IMPORT_TEMP_DESC` text,
  `IMPORT_TEMP_LINKMAN` varchar(32) DEFAULT NULL,
  `IMPORT_TEMP_CONTACT_ONE` varchar(256) DEFAULT NULL,
  `IMPORT_TEMP_CONTACT_TWO` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`IMPORT_TEMP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_import_temp
-- ----------------------------

-- ----------------------------
-- Table structure for crm_my_tasks
-- ----------------------------
DROP TABLE IF EXISTS `crm_my_tasks`;
CREATE TABLE `crm_my_tasks` (
  `TASK_ID` char(36) NOT NULL,
  `ORG_ID` char(36) DEFAULT NULL,
  `CUST_ID` char(36) DEFAULT NULL,
  `TASK_REVIEW_ID` char(36) DEFAULT NULL,
  `SALE_ID` char(36) DEFAULT NULL,
  `TASK_FOLLOW_STATE` varchar(32) DEFAULT NULL,
  `TASK_CLASS` varchar(32) DEFAULT NULL,
  `TASK_CREATE_TIME` varchar(32) DEFAULT NULL,
  `TASK_FINISH_TIME` varchar(32) DEFAULT NULL,
  `TASK_CUST_STATE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`TASK_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_my_tasks
-- ----------------------------

-- ----------------------------
-- Table structure for crm_opp_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_opp_info`;
CREATE TABLE `crm_opp_info` (
  `OPP_ID` char(36) NOT NULL,
  `OPP_NAME` varchar(32) DEFAULT NULL,
  `CLUE_ID` char(36) DEFAULT NULL,
  `CUST_ID` char(36) DEFAULT NULL,
  `CONT_ID` char(36) DEFAULT NULL,
  `OPP_DES` varchar(256) DEFAULT NULL,
  `OPP_CONCERN_PRODUCT` varchar(256) DEFAULT NULL,
  `OPP_EXPECT_INVEST` decimal(10,2) DEFAULT NULL,
  `OPP_START_TIME` varchar(32) DEFAULT NULL,
  `OPP_STATE` varchar(32) DEFAULT NULL,
  `CLUE_SALESMAN` char(36) DEFAULT NULL,
  `OPP_CREATER` char(36) DEFAULT NULL,
  `OPP_CREATE_TIME` varchar(32) DEFAULT NULL,
  `OPP_LEVEL` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`OPP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_opp_info
-- ----------------------------

-- ----------------------------
-- Table structure for crm_order_entry
-- ----------------------------
DROP TABLE IF EXISTS `crm_order_entry`;
CREATE TABLE `crm_order_entry` (
  `ENTRY_ID` char(36) NOT NULL,
  `ORDER_ID` char(36) DEFAULT NULL,
  `ENTRY_ORDER_PRODUCT` varchar(32) DEFAULT NULL,
  `ENTRY_PRODUCT_MODEL` varchar(32) DEFAULT NULL,
  `ENTRY_NUMBER` decimal(6,0) DEFAULT NULL,
  `ENTRY_UNIT_PRICE` decimal(10,2) DEFAULT NULL,
  `ENTRY_DISCOUNT` decimal(3,2) DEFAULT NULL,
  `ENTRY_REAL_PRICE` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`ENTRY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_order_entry
-- ----------------------------

-- ----------------------------
-- Table structure for crm_order_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_order_info`;
CREATE TABLE `crm_order_info` (
  `ORDER_ID` char(36) NOT NULL,
  `OPP_ID` char(36) DEFAULT NULL,
  `ORDER_CHIEF` varchar(32) DEFAULT NULL,
  `ORDER_DELIVERY_COST` decimal(10,2) DEFAULT NULL,
  `ORDER_COST` decimal(10,2) DEFAULT NULL,
  `ORDER_DES` varchar(200) DEFAULT NULL,
  `ORDER_STATE` varchar(32) DEFAULT NULL,
  `ORDER_CREATER` char(36) DEFAULT NULL,
  `ORDER_CREATE_TIME` varchar(32) DEFAULT NULL,
  `ORDER_CONFIRM_PERSON` char(36) DEFAULT NULL,
  `ORDER_CONFIRM_TIME` varchar(32) DEFAULT NULL,
  `CLUE_SALESMAN` char(36) DEFAULT NULL,
  PRIMARY KEY (`ORDER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_order_info
-- ----------------------------

-- ----------------------------
-- Table structure for crm_org_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_org_info`;
CREATE TABLE `crm_org_info` (
  `ORG_ID` char(36) NOT NULL,
  `ORG_NAME` varchar(32) DEFAULT NULL,
  `ORG_TYPE` varchar(32) DEFAULT NULL,
  `ORG_INTRODUCTION` text,
  `ORG_LINKMAN_NAME` varchar(32) DEFAULT NULL,
  `ORG_EMAIL` varchar(32) DEFAULT NULL,
  `ORG_CONTACT_WAY` varchar(256) DEFAULT NULL,
  `ORG_STATE` varchar(32) DEFAULT NULL,
  `ORG_LABELS` varchar(32) DEFAULT NULL,
  `ORG_ADDRESS` varchar(256) DEFAULT NULL,
  `ORG_WEBSITE` varchar(128) DEFAULT NULL,
  `ORG_CREATER` char(36) DEFAULT NULL,
  `ORG_CREATE_TIME` varchar(32) DEFAULT NULL,
  `ORG_UPDATE_TIME` varchar(32) DEFAULT NULL,
  `ORG_CLASSIFICATION` varchar(32) DEFAULT NULL,
  `ORG_VISIT_AGAIN_TIME` varchar(32) DEFAULT NULL,
  `CUST_ID` char(36) DEFAULT NULL,
  `ORG_SOURCES` varchar(32) DEFAULT NULL,
  `ORG_SALESMAN` char(36) DEFAULT NULL,
  PRIMARY KEY (`ORG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_org_info
-- ----------------------------

-- ----------------------------
-- Table structure for crm_per_info
-- ----------------------------
DROP TABLE IF EXISTS `crm_per_info`;
CREATE TABLE `crm_per_info` (
  `PER_ID` char(36) NOT NULL,
  `PER_NAME` varchar(32) DEFAULT NULL,
  `PER_SEX` varchar(10) DEFAULT NULL,
  `PER_EMAIL` varchar(32) DEFAULT NULL,
  `PER_CONTACT_WAY` varchar(256) DEFAULT NULL,
  `PER_STATE` varchar(32) DEFAULT NULL,
  `PER_LABELS` varchar(32) DEFAULT NULL,
  `PER_BELONG_ORG` varchar(32) DEFAULT NULL,
  `PER_CREATER` char(36) DEFAULT NULL,
  `PER_CREATE_TIME` varchar(32) DEFAULT NULL,
  `PER_UPDATE_TIME` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`PER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_per_info
-- ----------------------------

-- ----------------------------
-- Table structure for crm_procust_visit
-- ----------------------------
DROP TABLE IF EXISTS `crm_procust_visit`;
CREATE TABLE `crm_procust_visit` (
  `PROCUST_VISIT_ID` char(36) NOT NULL,
  `ORG_ID` char(36) DEFAULT NULL,
  `PROCUST_VISIT_DATE` date DEFAULT NULL,
  `PROCUST_VISIT_REMARK` varchar(1024) DEFAULT NULL,
  `PROCUST_VISIT_FILL_ID` char(36) DEFAULT NULL,
  `PROCUST_VISIT_FILL_TIME` datetime DEFAULT NULL,
  `PROCUST_VISIT_EFFECT` varchar(32) DEFAULT NULL,
  `PROCUST_VISIT_CUST_FOCUS` varchar(1024) DEFAULT NULL,
  `PROCUST_VISIT_TYPE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`PROCUST_VISIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_procust_visit
-- ----------------------------

-- ----------------------------
-- Table structure for crm_task_cycle
-- ----------------------------
DROP TABLE IF EXISTS `crm_task_cycle`;
CREATE TABLE `crm_task_cycle` (
  `TC_ID` char(36) NOT NULL,
  `TC_BEGIN` varchar(32) DEFAULT NULL,
  `TC_END` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`TC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_task_cycle
-- ----------------------------
INSERT INTO `crm_task_cycle` VALUES ('45B6EBB1-0C7C-4C2E-96BE-44B0F2443D04', '2017-03-20', '2017-03-26');
INSERT INTO `crm_task_cycle` VALUES ('7701885C-5244-4B3D-BE8A-497CDF6F6EC9', '2017-03-06', '2017-03-12');
INSERT INTO `crm_task_cycle` VALUES ('81404D46-ECD8-11E6-B6AA-46064C6D3DEB', '2017-01-30', '2017-02-05');
INSERT INTO `crm_task_cycle` VALUES ('85911B2D-705C-4043-8C65-0F46E9F05C52', '2017-03-27', '2017-04-02');
INSERT INTO `crm_task_cycle` VALUES ('8BC8BFE3-ECD8-11E6-B6AA-46064C6D3DEB', '2017-02-06', '2017-02-12');
INSERT INTO `crm_task_cycle` VALUES ('96679EFF-ECD8-11E6-B6AA-46064C6D3DEB', '2017-01-23', '2017-01-29');
INSERT INTO `crm_task_cycle` VALUES ('A5F623C3-12B8-4688-B777-8D8CD2DBDFA0', '2017-02-27', '2017-03-05');
INSERT INTO `crm_task_cycle` VALUES ('ADDD4E36-ECD8-11E6-B6AA-46064C6D3DEB', '2017-02-13', '2017-02-19');
INSERT INTO `crm_task_cycle` VALUES ('B41D3C7A-ECD8-11E6-B6AA-46064C6D3DEB', '2017-02-20', '2017-02-26');
INSERT INTO `crm_task_cycle` VALUES ('E787CCC2-1659-4E41-AF94-7109075158B7', '2017-03-13', '2017-03-19');

-- ----------------------------
-- Table structure for crm_task_review
-- ----------------------------
DROP TABLE IF EXISTS `crm_task_review`;
CREATE TABLE `crm_task_review` (
  `TASK_REVIEW_ID` char(36) NOT NULL,
  `TC_ID` char(36) DEFAULT NULL,
  `SALE_ID` char(36) DEFAULT NULL,
  `TASK_REVIEW_STATE` varchar(32) DEFAULT NULL,
  `TASK_REVIEW_DESC` text,
  `TASK_REVIEW_VISITS_TOTAL` int(11) DEFAULT NULL,
  `TASK_REVIEW_STRANGE` int(11) DEFAULT NULL,
  `TASK_REVIEW_RETURNVISIT` int(11) DEFAULT NULL,
  `TASK_REVIEW_NEW_INTENCUST` int(11) DEFAULT NULL,
  `TASK_REVIEW_RETURNVISIT_HIST` int(11) DEFAULT NULL,
  `TASK_REVIEW_LOSS` int(11) DEFAULT NULL,
  `TASK_REVIEW_STRANGE_TASK` int(11) DEFAULT NULL,
  `TASK_REVIEW_STRANGE_FOLLOW` int(11) DEFAULT NULL,
  `TASK_REVIEW_STRANGE_VISIT` int(11) DEFAULT NULL,
  `TASK_REVIEW_INTENTION_TASK` int(11) DEFAULT NULL,
  `TASK_REVIEW_INTENTION_FOLLOW` int(11) DEFAULT NULL,
  `TASK_REVIEW_INTENTION_VISIT` int(11) DEFAULT NULL,
  `OPP_NUM` int(11) DEFAULT NULL,
  `ORDER_NUM` int(11) DEFAULT NULL,
  `TASK_REVIEW_STRANGE_ACTUAL` int(11) DEFAULT NULL,
  `TASK_REVIEW_INTENTION_ACTUAL` int(11) DEFAULT NULL,
  PRIMARY KEY (`TASK_REVIEW_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_task_review
-- ----------------------------

-- ----------------------------
-- Table structure for crm_visit
-- ----------------------------
DROP TABLE IF EXISTS `crm_visit`;
CREATE TABLE `crm_visit` (
  `VISIT_ID` char(36) NOT NULL,
  `VISIT_TYPE` varchar(32) DEFAULT NULL,
  `VISIT_CUST_ID` char(36) DEFAULT NULL,
  `VISIT_RECEPTION_NAME` char(36) DEFAULT NULL,
  `VISIT_RECEPTION_SEX` varchar(32) DEFAULT NULL,
  `VISIT_RECEPTION_JOB` varchar(32) DEFAULT NULL,
  `VISIT_RECEPTION_PHONE` varchar(128) DEFAULT NULL,
  `VISIT_USER_ID` char(36) DEFAULT NULL,
  `VISIT_PEER_NAME` varchar(32) DEFAULT NULL,
  `VISIT_DATE` datetime DEFAULT NULL,
  `VISIT_CONTENT` varchar(256) DEFAULT NULL,
  `VISIT_CUST_FOCUS` varchar(256) DEFAULT NULL,
  `VISIT_EFFECT` varchar(32) DEFAULT NULL,
  `VISIT_IMPROVEMENT` varchar(32) DEFAULT NULL,
  `VISIT_COST` decimal(10,2) DEFAULT NULL,
  `VISIT_COST_EXPLAIN` varchar(256) DEFAULT NULL,
  `VISIT_STATE` varchar(32) DEFAULT NULL,
  `VISIT_FILL_ID` char(36) DEFAULT NULL,
  `VISIT_FILL_TIME` datetime DEFAULT NULL,
  `VISIT_CONFIRM_ID` char(36) DEFAULT NULL,
  `VISIT_CONFIRM_TIME` datetime DEFAULT NULL,
  PRIMARY KEY (`VISIT_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of crm_visit
-- ----------------------------

-- ----------------------------
-- Table structure for security_group
-- ----------------------------
DROP TABLE IF EXISTS `security_group`;
CREATE TABLE `security_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_CODE` varchar(32) DEFAULT NULL,
  `GRP_NAME` varchar(32) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  `GRP_STATE` varchar(1) DEFAULT NULL,
  `GRP_SORT` int(11) DEFAULT NULL,
  `GRP_TYPE` varchar(32) DEFAULT NULL,
  `GRP_RANK` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group
-- ----------------------------
INSERT INTO `security_group` VALUES ('00000000-0000-0000-00000000000000000', 'Root', '公司集团', null, null, '1', '0', 'company', '1');
INSERT INTO `security_group` VALUES ('0BE47DDB-862E-45F3-8E43-0A4990847D8B', 'ITT', 'IT部', '22375139-38A8-4969-81CA-915BEC3E58D2', '', '1', '2', 'department', '1');
INSERT INTO `security_group` VALUES ('22375139-38A8-4969-81CA-915BEC3E58D2', 'ShenYang', '沈阳分公司', '00000000-0000-0000-00000000000000000', '', '1', '4', 'company', '1');
INSERT INTO `security_group` VALUES ('995F3F7D-3D9F-4533-922C-F445F9ED59C2', 'SALE', '营销部', '22375139-38A8-4969-81CA-915BEC3E58D2', '', '1', '3', 'department', '1');
INSERT INTO `security_group` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'IT', '信息部', '22375139-38A8-4969-81CA-915BEC3E58D2', '', '1', '1', 'department', '1');

-- ----------------------------
-- Table structure for security_group_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_group_auth`;
CREATE TABLE `security_group_auth` (
  `GRP_AUTH_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`GRP_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_group_auth
-- ----------------------------
INSERT INTO `security_group_auth` VALUES ('4C20EDAC-0A0B-45BB-8B71-B557A94E7429', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'Navigater', '02');

-- ----------------------------
-- Table structure for security_rg_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_rg_auth`;
CREATE TABLE `security_rg_auth` (
  `RG_AUTH_ID` char(36) NOT NULL,
  `RG_ID` char(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` char(36) DEFAULT NULL,
  PRIMARY KEY (`RG_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_rg_auth
-- ----------------------------
INSERT INTO `security_rg_auth` VALUES ('025E2C9B-92A2-4B70-8CA4-DDA8A47C3427', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Operation', '2A03F130-73A4-4A11-A03A-121BE4B1BFA0');
INSERT INTO `security_rg_auth` VALUES ('035D59B8-8749-4BD3-AE0D-BC35E4988E4E', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_rg_auth` VALUES ('0C333091-C453-4850-AA37-97678750A707', '20618096-71D5-4A7B-B745-4AC0DCEB3950', 'Handler', '2287114B-17C2-4E88-8A2F-505709ADB719');
INSERT INTO `security_rg_auth` VALUES ('1B12BB78-C3ED-4767-BA0B-460A1411684B', '2BB576EF-5471-40B0-BC08-5565961CF954', 'Operation', '7211605C-F7D5-4C45-8918-B3FC938F5EF5');
INSERT INTO `security_rg_auth` VALUES ('2FA78219-1682-4E70-8FCE-2E6D3E78D85B', '20618096-71D5-4A7B-B745-4AC0DCEB3950', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_rg_auth` VALUES ('34E4F6C1-19DE-4069-B50E-153518A0E609', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_rg_auth` VALUES ('35028B8C-C830-4A9C-81E0-C5CCDBF4EA01', '20618096-71D5-4A7B-B745-4AC0DCEB3950', 'Menu', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `security_rg_auth` VALUES ('39D8E308-5F64-404C-B4E9-ED5800388BF8', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Menu', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `security_rg_auth` VALUES ('43B55026-B224-4CF0-9B5A-EE264A951364', '20618096-71D5-4A7B-B745-4AC0DCEB3950', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_rg_auth` VALUES ('4CC4E334-735A-49FB-BEC8-8FBBCB3E4AEB', '2BB576EF-5471-40B0-BC08-5565961CF954', 'Handler', '2287114B-17C2-4E88-8A2F-505709ADB719');
INSERT INTO `security_rg_auth` VALUES ('4F16B0BE-263A-4BF4-8CB9-30D234B0D65C', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Operation', '2CAE2167-AA7B-437D-8953-C0E392B15AFC');
INSERT INTO `security_rg_auth` VALUES ('519E7F64-AEE5-4B4B-BC5E-FFE96771B073', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Handler', '2287114B-17C2-4E88-8A2F-505709ADB719');
INSERT INTO `security_rg_auth` VALUES ('56E4E566-AD6A-4921-BB11-E494F8A0ADBE', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Menu', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `security_rg_auth` VALUES ('5EA043C0-5D23-41D8-BEA8-14340C5DE5EF', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Handler', '2287114B-17C2-4E88-8A2F-505709ADB719');
INSERT INTO `security_rg_auth` VALUES ('5F3FA65A-F4C1-4C38-B13D-39652BE9D87B', '20618096-71D5-4A7B-B745-4AC0DCEB3950', 'Operation', '6D3B756C-739B-4075-A69C-750F539223B3');
INSERT INTO `security_rg_auth` VALUES ('5F8D7D4C-51F3-4F8E-B500-AD1C8CDA20BA', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Operation', '2A03F130-73A4-4A11-A03A-121BE4B1BFA0');
INSERT INTO `security_rg_auth` VALUES ('649BBA62-302D-4FE3-9F40-91A124721CD2', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Operation', '7211605C-F7D5-4C45-8918-B3FC938F5EF5');
INSERT INTO `security_rg_auth` VALUES ('6558A9C1-850D-453D-AB1F-6C7C8D737135', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Operation', 'BACCCF29-7D2E-4C49-A03A-F1F63DDBBA15');
INSERT INTO `security_rg_auth` VALUES ('725027AB-394E-407C-918C-7A742FA24B85', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Operation', '7211605C-F7D5-4C45-8918-B3FC938F5EF5');
INSERT INTO `security_rg_auth` VALUES ('75C2A4E6-7448-49EA-B9FA-10BBFA14F29B', '2BB576EF-5471-40B0-BC08-5565961CF954', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_rg_auth` VALUES ('7EFA20E8-7341-431E-847D-DB1ACF59A53A', '2BB576EF-5471-40B0-BC08-5565961CF954', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_rg_auth` VALUES ('8630989D-4434-4C13-9586-867C3AFBCABF', '2BB576EF-5471-40B0-BC08-5565961CF954', 'Operation', '2A03F130-73A4-4A11-A03A-121BE4B1BFA0');
INSERT INTO `security_rg_auth` VALUES ('95D6712B-079D-4629-A021-905CA461CC63', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Operation', 'BACCCF29-7D2E-4C49-A03A-F1F63DDBBA15');
INSERT INTO `security_rg_auth` VALUES ('9EA5B828-9D29-4635-BB92-D1C31B91FC9E', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Operation', '790FA246-8461-4607-A046-903A2C8DE118');
INSERT INTO `security_rg_auth` VALUES ('B55910CC-328C-4D62-BDB0-40B9FAC5749D', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Operation', '2CAE2167-AA7B-437D-8953-C0E392B15AFC');
INSERT INTO `security_rg_auth` VALUES ('CEBF8650-BAA0-41CC-940F-B9C731C199C1', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Operation', '6D3B756C-739B-4075-A69C-750F539223B3');
INSERT INTO `security_rg_auth` VALUES ('D72FA33F-C381-45C1-B744-1320DC8A283A', '2BB576EF-5471-40B0-BC08-5565961CF954', 'Operation', '2CAE2167-AA7B-437D-8953-C0E392B15AFC');
INSERT INTO `security_rg_auth` VALUES ('E1BF7CD7-5B5E-49F8-9A22-01CE9A6E1FB4', '2BB576EF-5471-40B0-BC08-5565961CF954', 'Menu', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `security_rg_auth` VALUES ('E82DA951-3E23-43A5-BA6B-3C5AAAAEDDD9', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_rg_auth` VALUES ('ECE69F74-7C84-445E-BB77-2625708AD1BF', 'A1F3C828-0256-4D0C-AC95-589C58B4032E', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_rg_auth` VALUES ('F1EC56B5-2F6D-43F3-99B0-DD7F10DD6EBD', '20618096-71D5-4A7B-B745-4AC0DCEB3950', 'Operation', '7211605C-F7D5-4C45-8918-B3FC938F5EF5');
INSERT INTO `security_rg_auth` VALUES ('F8CDF445-BB49-4AFA-8E8A-1C4FF077965F', '2BB576EF-5471-40B0-BC08-5565961CF954', 'Operation', 'BACCCF29-7D2E-4C49-A03A-F1F63DDBBA15');

-- ----------------------------
-- Table structure for security_role
-- ----------------------------
DROP TABLE IF EXISTS `security_role`;
CREATE TABLE `security_role` (
  `ROLE_ID` varchar(36) NOT NULL,
  `ROLE_CODE` varchar(32) DEFAULT NULL,
  `ROLE_NAME` varchar(32) DEFAULT NULL,
  `ROLE_PID` varchar(36) DEFAULT NULL,
  `ROLE_DESC` varchar(128) DEFAULT NULL,
  `ROLE_STATE` varchar(32) DEFAULT NULL,
  `ROLE_SORT` int(11) DEFAULT NULL,
  `ROLE_TYPE` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role
-- ----------------------------
INSERT INTO `security_role` VALUES ('00000000-0000-0000-00000000000000000', 'System', '系统角色', null, null, '1', null, 'menu');
INSERT INTO `security_role` VALUES ('05FC4E42-2BCF-4651-BDFF-86444D055695', 'SALES_MANAGER', '销售经理', '00000000-0000-0000-00000000000000000', '', '1', '7', 'part');
INSERT INTO `security_role` VALUES ('8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'SALESMAN', '销售人员', '00000000-0000-0000-00000000000000000', '', '1', '7', 'part');
INSERT INTO `security_role` VALUES ('9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'SALES_ASSISTANT', '销售助理', '00000000-0000-0000-00000000000000000', '', '1', '9', 'part');
INSERT INTO `security_role` VALUES ('DDA88E11-231C-43EC-9657-7AA55D842407', 'SALES_DIRECTOR', '销售总监', '00000000-0000-0000-00000000000000000', '', '1', '6', 'part');

-- ----------------------------
-- Table structure for security_role_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_role_auth`;
CREATE TABLE `security_role_auth` (
  `ROLE_AUTH_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ROLE_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_auth
-- ----------------------------
INSERT INTO `security_role_auth` VALUES ('0189942A-B628-4C3A-8BAE-433D054ED4A4', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'AD843A54-9F8B-4F34-A5E3-6C2BF0537381');
INSERT INTO `security_role_auth` VALUES ('0361DDF2-0F68-4326-8D15-89B79D754A91', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '093413BA-1ABC-464F-AEA0-13FF82591D22');
INSERT INTO `security_role_auth` VALUES ('036AA113-83F6-411D-8CEC-1D4BEDEDEDC7', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '9E812AE0-CFE4-4891-B7BF-22A22B643543');
INSERT INTO `security_role_auth` VALUES ('0578EB32-4232-4DBE-AD95-ECBC27B5F28B', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '1BBAA470-C306-4915-9ECC-EBCF72EDC38D');
INSERT INTO `security_role_auth` VALUES ('06A46DA8-CA9C-41E5-807A-D28E18A0BFA5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'C5D919FA-A812-4AB2-AED5-81BE5B31B79D');
INSERT INTO `security_role_auth` VALUES ('09D892FE-95DA-4AF9-A890-3ACF05436D12', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('0A01E0D0-4DCC-4E7A-8C61-7F96EC5F928F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '9F4DFA03-9114-4268-87C6-26B8EE3606D3');
INSERT INTO `security_role_auth` VALUES ('0B411017-5F0B-46F2-8D01-7A5902846973', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1B18D08B-2066-4259-A9AE-4987FB58C3E4');
INSERT INTO `security_role_auth` VALUES ('0C839172-5308-46FC-9ED6-B6F75A1C5080', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'EC67A415-CC8F-45B5-A28D-797D84435841');
INSERT INTO `security_role_auth` VALUES ('0CAB8F08-B627-4BD7-856E-3EE5B00762A2', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D');
INSERT INTO `security_role_auth` VALUES ('0CACE3F0-D705-4385-9849-8FF76450AA74', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '8EF8E532-1B93-440D-B0D6-680D7F72207B');
INSERT INTO `security_role_auth` VALUES ('0CC962DF-F7AF-4AA6-AFC5-244BB811B33A', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '85705567-2B39-4661-8995-4013A7E80E76');
INSERT INTO `security_role_auth` VALUES ('0E854BF9-3B4F-4B95-83EC-173DFABE3E6E', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '37FEF4ED-0F0A-404F-9107-2329D4440A48');
INSERT INTO `security_role_auth` VALUES ('0EE82AE7-6FA1-40DA-885C-2B7EF5D0534C', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'E6CFF307-AD95-42FA-8234-C551BDB7BFA1');
INSERT INTO `security_role_auth` VALUES ('0F1E1CE6-2930-4D4C-B181-69201865F02B', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'AEA07454-68B1-453D-ABC3-C6CD10026CFA');
INSERT INTO `security_role_auth` VALUES ('0F8A9C81-A4A5-4605-9CE5-0B70675133C4', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('0FD821A0-00C2-4376-A8D1-E8B47ADDD25F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'AEEDFDAB-7802-492D-B369-2DEE350208B5');
INSERT INTO `security_role_auth` VALUES ('10877B4A-AFB3-4FB2-AC3A-455039F19C48', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '495A111E-EE16-452E-92D9-56CF97BD6388');
INSERT INTO `security_role_auth` VALUES ('113B0A4A-17FD-473C-BD15-D563268C2E9F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '85705567-2B39-4661-8995-4013A7E80E76');
INSERT INTO `security_role_auth` VALUES ('1186C4CA-3F2A-47E2-B9CD-34848E180BF3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '1FEEF11B-605F-432E-83BE-AA2173C0EC59');
INSERT INTO `security_role_auth` VALUES ('12C18802-7C8E-4028-99D4-F0142874D056', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '452593FA-A9D6-4F77-B880-21F97E9688D1');
INSERT INTO `security_role_auth` VALUES ('12D913FD-D7FD-495E-A540-632314E49063', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'AEEDFDAB-7802-492D-B369-2DEE350208B5');
INSERT INTO `security_role_auth` VALUES ('148F5451-1605-4BB0-BC7D-DD448E28F108', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '2CAE2167-AA7B-437D-8953-C0E392B15AFC');
INSERT INTO `security_role_auth` VALUES ('165846DF-C481-457C-92AD-729F25B58C8D', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '4E2CAF22-3DCB-415D-ABC5-0E6BC2B50BFE');
INSERT INTO `security_role_auth` VALUES ('166AA75A-77A0-4FBE-B7AC-6DA605B96615', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '7D68E2A0-C40D-46BE-99B5-4BE51947386D');
INSERT INTO `security_role_auth` VALUES ('16F8CD76-FA66-42DF-AB32-E3A7F83BEB6D', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '579FCABC-B5D5-4157-91DD-D638943C3BCF');
INSERT INTO `security_role_auth` VALUES ('174C4AA2-0967-4C91-9BF5-B1DE91EE5A19', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');
INSERT INTO `security_role_auth` VALUES ('17769E2A-A907-44F2-B42A-44C7BEF1D870', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', '76FF0E56-DA31-4399-82BF-8B1C9B253872');
INSERT INTO `security_role_auth` VALUES ('185D8307-6A9E-4C78-AA19-879865E31BC6', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'FF12FE7A-BD11-448A-BCC9-E0D23FAA33A6');
INSERT INTO `security_role_auth` VALUES ('1B7256BC-B623-4228-A6C7-EBD03CBF42D3', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7');
INSERT INTO `security_role_auth` VALUES ('1B7AF7EB-129A-45F3-8976-0634243E2974', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `security_role_auth` VALUES ('1B9AB8FD-5747-49AE-834B-AC5B3D58A22E', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('1DD59D29-2172-4701-90F8-19C629B1646E', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '0831D3B9-7BD5-4581-A540-72982B899171');
INSERT INTO `security_role_auth` VALUES ('1DF5DF63-79E3-401B-A993-30EB335BF1B1', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '52C7239C-39B8-4CCC-94A9-1A2BFD7B48A0');
INSERT INTO `security_role_auth` VALUES ('1E827FE9-C6C2-43A7-8197-C71AEA443FA0', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '2321E4A1-2CB5-430C-8841-5129D02D2C38');
INSERT INTO `security_role_auth` VALUES ('1E93950A-FF7E-428B-905A-26EF19591B35', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'FEE4BDAE-A1ED-47E1-9065-22285EB8F57B');
INSERT INTO `security_role_auth` VALUES ('1FF5DC1D-34A2-4259-9915-944248B4CEBE', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '00042A5A-FF5C-4EF2-89BB-5A6CB2D7EDA6');
INSERT INTO `security_role_auth` VALUES ('20DFEE49-FF2D-47CD-9A1B-7F9E1F6F7D2F', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '37FEF4ED-0F0A-404F-9107-2329D4440A48');
INSERT INTO `security_role_auth` VALUES ('21002270-8007-4D16-9716-AAB03631E200', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '12691874-9834-440C-929A-87C699B7AA4E');
INSERT INTO `security_role_auth` VALUES ('21094A57-68C1-4BE9-B9AA-4AA52D32BC92', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Handler', '3743767A-8D65-4466-B0A3-D2C11F07B5EC');
INSERT INTO `security_role_auth` VALUES ('227424B1-B4D4-4E76-B2E3-884802C3E908', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '6A6591AB-3717-4F50-A609-18B332413E86');
INSERT INTO `security_role_auth` VALUES ('25215598-B0A3-4263-977E-11DF52F13E77', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1BF7D254-304B-4719-BCB6-AB38BF8CA4E7');
INSERT INTO `security_role_auth` VALUES ('273B2506-5A40-4F6A-8941-3C259DBF6113', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'FEE4BDAE-A1ED-47E1-9065-22285EB8F57B');
INSERT INTO `security_role_auth` VALUES ('27A3CD98-ED50-4212-9E6D-FF2572E516F0', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '1BBAA470-C306-4915-9ECC-EBCF72EDC38D');
INSERT INTO `security_role_auth` VALUES ('28916250-722B-4B23-AB62-681EC9CE0027', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Handler', '3890BD46-F782-4036-9FC3-E6DA46140C5D');
INSERT INTO `security_role_auth` VALUES ('2A245C6F-F5ED-40F3-A487-DDC52DC96815', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA');
INSERT INTO `security_role_auth` VALUES ('2A6843F6-E2A4-4D3C-AC8A-7342B936A86E', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '02E89343-F996-44EC-8347-6FB3C60BC9BB');
INSERT INTO `security_role_auth` VALUES ('2AD16514-3C1F-4DFD-BB24-C2D8EBCD734F', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '3C03348B-D91C-4A35-B540-7BB67A153463');
INSERT INTO `security_role_auth` VALUES ('2AD28B8F-C321-42BC-A6B7-443258A33996', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '496E22A5-67BC-40C9-9F99-6FDB6193B441');
INSERT INTO `security_role_auth` VALUES ('2B60A874-CF04-43A5-8EC2-11F97665D58E', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '7211605C-F7D5-4C45-8918-B3FC938F5EF5');
INSERT INTO `security_role_auth` VALUES ('2BE721EA-A7F8-4395-A153-5AA0443E315B', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '3B39B662-671E-4ECF-B14D-AE8F4FDC335D');
INSERT INTO `security_role_auth` VALUES ('2E240D78-60A4-48BB-8631-F6532E4F7CCE', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '82384F1D-4CD1-4DF3-9121-B80EE0B15973');
INSERT INTO `security_role_auth` VALUES ('30A683EE-104A-49C9-A402-1D8C5A51C041', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '17FB686F-8580-4995-8494-EC42F9C71FC0');
INSERT INTO `security_role_auth` VALUES ('3193EF52-E1DB-419C-BC24-5E5CB98D3CFB', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'AD843A54-9F8B-4F34-A5E3-6C2BF0537381');
INSERT INTO `security_role_auth` VALUES ('32C77339-DC6B-4050-9414-23C713B3DA2B', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('3597DE6B-D190-47F3-AC0D-236709330946', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '6224A19C-0FC4-439F-B5C8-79E86557B29E');
INSERT INTO `security_role_auth` VALUES ('36363324-DBFB-4439-AA39-CB7F06A089C6', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `security_role_auth` VALUES ('378C2F9C-07F6-49FB-B390-35288E21EF16', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'E9F11BB9-A6CC-47AB-B475-B7B54D898C8D');
INSERT INTO `security_role_auth` VALUES ('3886F0FB-6D69-4687-BCCE-1FA421A1844D', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '910D3B2D-0019-4FAD-8593-43710B416A3B');
INSERT INTO `security_role_auth` VALUES ('38BCF258-9922-4BD9-ADD5-9D2CF0990D8C', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '8EF8E532-1B93-440D-B0D6-680D7F72207B');
INSERT INTO `security_role_auth` VALUES ('396B9732-ECEC-4277-8B77-A87C54FAB300', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Menu', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `security_role_auth` VALUES ('39D6AB71-9BB9-4E0E-B594-B01D5512D3C0', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '2CAE2167-AA7B-437D-8953-C0E392B15AFC');
INSERT INTO `security_role_auth` VALUES ('3B187E11-5194-4F57-9987-C400DBE62966', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'D28C6156-32D2-44C4-90D4-63DC05CD6407');
INSERT INTO `security_role_auth` VALUES ('3CB5CDB1-7405-4D7C-BDFE-A8F7E1DFBA6A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '2B49A370-5B78-4FFB-8792-1BB61DBA9478');
INSERT INTO `security_role_auth` VALUES ('3CD305C8-7AB1-4F61-AC8C-8C07AD3EDA75', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '7D68E2A0-C40D-46BE-99B5-4BE51947386D');
INSERT INTO `security_role_auth` VALUES ('3D5CA9B6-A6A9-41DE-AC26-CE9FA601986B', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57');
INSERT INTO `security_role_auth` VALUES ('3F1D25DB-A95C-471E-8763-B60F58E50C70', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '3890BD46-F782-4036-9FC3-E6DA46140C5D');
INSERT INTO `security_role_auth` VALUES ('408932A2-1089-4E26-83E0-06A998884CE9', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('40F9D867-7D08-4EFB-9DEF-D1F2B9FF2B46', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `security_role_auth` VALUES ('426985C2-5480-4245-8B9C-740EEFC4AAB3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '6C16A2E5-0128-4D93-84C3-E9BB9F2327B6');
INSERT INTO `security_role_auth` VALUES ('44D70975-8A7E-4D7B-A601-DBB18530CBD0', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `security_role_auth` VALUES ('46301688-59EF-454F-9BBB-130CC1FFC1BE', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '00042A5A-FF5C-4EF2-89BB-5A6CB2D7EDA6');
INSERT INTO `security_role_auth` VALUES ('4687C8BA-D9E0-4504-8E1F-9320D0DF159E', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22');
INSERT INTO `security_role_auth` VALUES ('4751932E-04BF-4B9A-926A-B83FCDD9D4CB', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '495A111E-EE16-452E-92D9-56CF97BD6388');
INSERT INTO `security_role_auth` VALUES ('47EC6ADC-0889-4951-9CAB-D2A3CD87F046', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '4E2CAF22-3DCB-415D-ABC5-0E6BC2B50BFE');
INSERT INTO `security_role_auth` VALUES ('48A80F80-9F43-437A-B64E-2E8D8C140E35', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '35067AE0-9243-450A-B9D7-59B550CDD8D2');
INSERT INTO `security_role_auth` VALUES ('4A09B7FF-6746-4AAA-BC14-253310FE23F0', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '7211605C-F7D5-4C45-8918-B3FC938F5EF5');
INSERT INTO `security_role_auth` VALUES ('4BC82B5F-4C09-4CF6-8E5B-AFD3184503CE', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'D413878A-2092-40D8-B946-1B6F90D2F194');
INSERT INTO `security_role_auth` VALUES ('4E0294C3-FAFE-42CD-8503-5EBFF26BF1E2', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '91E10CCF-A520-4712-A82C-ADCB8CFA0921');
INSERT INTO `security_role_auth` VALUES ('4F26A747-B9C9-4871-8253-8B352DFC3BD2', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '3B39B662-671E-4ECF-B14D-AE8F4FDC335D');
INSERT INTO `security_role_auth` VALUES ('4F275F7B-AB45-493D-8E74-37D516126EC9', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '2BF95B4C-B823-402B-AB8C-284CE7EDE678');
INSERT INTO `security_role_auth` VALUES ('50AE623E-732A-4598-9E8B-ED69DF93CC8F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'C8BEFAFB-0E45-431A-BCBC-1AB2819BCA91');
INSERT INTO `security_role_auth` VALUES ('50F035D3-E612-426B-A5D8-D8476CF423F7', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '04DA2B04-A1F5-43C5-B4DB-E9F7B156E8C7');
INSERT INTO `security_role_auth` VALUES ('51230009-0695-4129-A681-026050CE62B3', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'CF4DB6EE-7AD3-481D-90C0-125CA6329E4C');
INSERT INTO `security_role_auth` VALUES ('52EB63BC-B124-4CC9-A691-881293B23000', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '1DC1E347-7C48-4BE8-A271-9FDCE5A074BB');
INSERT INTO `security_role_auth` VALUES ('541DCB64-413F-4F0A-BD46-6684A7F5E1BD', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '567FBEE1-306B-4EC1-8688-906D5629C3D4');
INSERT INTO `security_role_auth` VALUES ('54680DD8-D52B-49E9-839C-BEF6E023C1E5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `security_role_auth` VALUES ('5503EB23-3717-402C-90C5-C0D7279EB8AC', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '2B49A370-5B78-4FFB-8792-1BB61DBA9478');
INSERT INTO `security_role_auth` VALUES ('55376C93-13F1-442F-B779-2236AAA44D21', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'A4F18B90-AEBA-4CE1-AA52-B534C4F79CA2');
INSERT INTO `security_role_auth` VALUES ('5584F70A-5CB4-469A-A458-94DE62A53B75', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'A80074A6-7BA6-479D-9CDD-768983177E06');
INSERT INTO `security_role_auth` VALUES ('55C51690-0435-4AB9-9ABA-8F4D9FB1BE37', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'F4C8FD10-7ED1-4912-B7A5-92DFA7070159');
INSERT INTO `security_role_auth` VALUES ('563DFA34-04D4-4AD4-872C-B292F14F63DF', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '35067AE0-9243-450A-B9D7-59B550CDD8D2');
INSERT INTO `security_role_auth` VALUES ('56A2E93A-A03B-459D-981B-CBE35EC4D5C9', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '9AA13920-43B2-4AEF-9D9C-9C2F71F34043');
INSERT INTO `security_role_auth` VALUES ('5801EF12-9EB7-49DE-ADB6-BB451AB779B1', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '5524CB68-469B-4163-99EF-1EDDE8E13AFD');
INSERT INTO `security_role_auth` VALUES ('5948DF31-C313-4CE3-8225-C4F3B8D8FC93', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'FFDB914C-B3F6-4871-8AEE-515A1081ACF0');
INSERT INTO `security_role_auth` VALUES ('594A7D4F-1769-4193-8A0A-A922434D7B14', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'CF4DB6EE-7AD3-481D-90C0-125CA6329E4C');
INSERT INTO `security_role_auth` VALUES ('59CF51CB-6560-496B-8933-B60CDD9C10D6', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '5524CB68-469B-4163-99EF-1EDDE8E13AFD');
INSERT INTO `security_role_auth` VALUES ('5A2B639F-6F9F-44E1-9764-C97E40BBB119', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'FCD061B9-45BC-4E7A-86CB-B179E30B0A76');
INSERT INTO `security_role_auth` VALUES ('5A354008-952A-48B4-BBD7-32B3ED3C4DC4', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '91E10CCF-A520-4712-A82C-ADCB8CFA0921');
INSERT INTO `security_role_auth` VALUES ('5B5BB82D-F0A4-4D47-99B9-38C7904F3C95', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '7D68E2A0-C40D-46BE-99B5-4BE51947386D');
INSERT INTO `security_role_auth` VALUES ('5BA86994-1C97-408B-8FD9-98F80650998D', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `security_role_auth` VALUES ('5D0D7824-5579-4AAF-B027-B40514A487A5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'CBD4685F-AC93-4574-95CB-BFF7E900BD2E');
INSERT INTO `security_role_auth` VALUES ('5D802ADD-DEE5-43CF-BB91-3075FE88219D', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', '3890BD46-F782-4036-9FC3-E6DA46140C5D');
INSERT INTO `security_role_auth` VALUES ('5EE7FFE7-7157-4DA6-BAFD-05146A2F8E97', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1F173085-5E15-4E9A-8F06-922E3E92BE95');
INSERT INTO `security_role_auth` VALUES ('5F6919CD-F40E-458E-ABD5-03231A11E73E', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8');
INSERT INTO `security_role_auth` VALUES ('60BB37EE-7853-42F0-87EB-D7AD48833316', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '1FEEF11B-605F-432E-83BE-AA2173C0EC59');
INSERT INTO `security_role_auth` VALUES ('60FAB83C-BC75-483A-87CF-87F4E603A864', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '863E15B1-7DF1-4A73-8F19-1F9CD0B7EFAB');
INSERT INTO `security_role_auth` VALUES ('621669BD-BEB8-4293-900D-CC8E3D387F35', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'E723E562-9B05-4778-8850-4DF9518B8DD4');
INSERT INTO `security_role_auth` VALUES ('6320BA21-822F-430D-A83C-9D1E3631D9E6', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', 'BACCCF29-7D2E-4C49-A03A-F1F63DDBBA15');
INSERT INTO `security_role_auth` VALUES ('6354DBD0-9763-496C-B32E-0FE8CB05EA06', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('6387EBD7-D350-40F1-9A59-7F1D9C1AEEC8', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');
INSERT INTO `security_role_auth` VALUES ('63BC894E-7529-43DE-BA06-FC92B6CB3226', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', 'F63CF10B-C61B-448B-BA04-1D07BD6927ED');
INSERT INTO `security_role_auth` VALUES ('67B01527-1687-45A4-BA09-CB81DB34A667', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `security_role_auth` VALUES ('68CD1CD1-CEB0-41C0-9AC0-6578BEAB6334', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', '894F3E00-FF69-494E-A072-5D63D185097E');
INSERT INTO `security_role_auth` VALUES ('69F6CDEA-417B-4793-81AC-AA454BEE6405', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '52C7239C-39B8-4CCC-94A9-1A2BFD7B48A0');
INSERT INTO `security_role_auth` VALUES ('6C1FF496-2EAD-4B2C-A36C-95D51297FAE4', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', 'F63CF10B-C61B-448B-BA04-1D07BD6927ED');
INSERT INTO `security_role_auth` VALUES ('6D152A74-5F69-4FD9-89A2-F095E601F7C4', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('6D1B721A-E602-4673-B384-6DF91713EFB7', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '1F173085-5E15-4E9A-8F06-922E3E92BE95');
INSERT INTO `security_role_auth` VALUES ('6D245581-031F-4AB7-B736-00096D2810EA', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '199DCC87-DB0D-4F78-A0AE-CCE348B13516');
INSERT INTO `security_role_auth` VALUES ('6D4E0A9E-7C44-4168-9D13-4F15DE7022A2', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'B8022643-0966-4493-84BA-23A77B09AD2C');
INSERT INTO `security_role_auth` VALUES ('6D81D9FA-5609-4A70-B778-2879BEE3C3F8', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1720253F-2F2E-403B-9D24-B96287063081');
INSERT INTO `security_role_auth` VALUES ('6D842560-F4F6-44AE-B8F9-6028F996998F', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Handler', 'F63CF10B-C61B-448B-BA04-1D07BD6927ED');
INSERT INTO `security_role_auth` VALUES ('712610DD-D196-43A1-926C-18B6E572B843', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '6D3B756C-739B-4075-A69C-750F539223B3');
INSERT INTO `security_role_auth` VALUES ('7192C6A1-5F2F-4530-95D5-4B9E16D6B68C', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Handler', 'F63CF10B-C61B-448B-BA04-1D07BD6927ED');
INSERT INTO `security_role_auth` VALUES ('73493AEF-F6C1-49D5-857A-14AFD54BC9D5', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'E723E562-9B05-4778-8850-4DF9518B8DD4');
INSERT INTO `security_role_auth` VALUES ('73718AD9-805B-4D4F-B7D4-02507DBABB75', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'B5CD6899-DECC-450D-A69E-BF82B0A5DAD9');
INSERT INTO `security_role_auth` VALUES ('7493C7F8-3F86-4E6E-AA0E-DEE3FBEC9D7A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'E9F11BB9-A6CC-47AB-B475-B7B54D898C8D');
INSERT INTO `security_role_auth` VALUES ('7660C7EE-978B-4976-97A3-6B671B7C6F18', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Menu', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `security_role_auth` VALUES ('769DF6CD-CE14-4D31-B70A-0370EA1BF63A', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '3C03348B-D91C-4A35-B540-7BB67A153463');
INSERT INTO `security_role_auth` VALUES ('77680B1E-380E-4E62-B263-DB44ECAC2675', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '12691874-9834-440C-929A-87C699B7AA4E');
INSERT INTO `security_role_auth` VALUES ('779B8D8E-9897-4F94-A762-3D28F85F8BD0', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D');
INSERT INTO `security_role_auth` VALUES ('7A1992FC-BD9D-4C9A-A22D-8338655334F5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'A3D11759-6C21-4B36-9BA2-2CE232C39085');
INSERT INTO `security_role_auth` VALUES ('7A404FB4-9590-442E-ADFA-9C6998DAEE3D', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '2A03F130-73A4-4A11-A03A-121BE4B1BFA0');
INSERT INTO `security_role_auth` VALUES ('7B9C4D1D-C802-4F30-97E8-35A8764BEE1E', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'A4F18B90-AEBA-4CE1-AA52-B534C4F79CA2');
INSERT INTO `security_role_auth` VALUES ('7BDC8BE5-4F98-45BA-8344-D0518AAAB499', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `security_role_auth` VALUES ('7BFE76F2-4098-4505-95AB-A9A2AA30E2DF', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '308FB9DB-AA23-4E5A-9D60-2D3BC0B28D7C');
INSERT INTO `security_role_auth` VALUES ('7CDC8203-CB66-4516-AD71-BA39C8E75FD7', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '3F844D55-E2AA-43C0-B795-88B5DE2440CF');
INSERT INTO `security_role_auth` VALUES ('7DA9B695-BD2E-489D-870E-E376B0D2C5D5', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22');
INSERT INTO `security_role_auth` VALUES ('7DB3F4AE-E42C-46AA-8652-E0F4A8BB895B', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '8A2C52B7-7F4C-415D-8C46-48AAD36D01D6');
INSERT INTO `security_role_auth` VALUES ('801D8A7E-21C0-4DA0-A630-F5BF42E3F0BD', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'A3D11759-6C21-4B36-9BA2-2CE232C39085');
INSERT INTO `security_role_auth` VALUES ('813055A6-C8D4-495F-A529-FBD6A41937D8', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '6C16A2E5-0128-4D93-84C3-E9BB9F2327B6');
INSERT INTO `security_role_auth` VALUES ('821E8871-3C44-4CAB-AB78-3E176FB2FF46', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'E6CFF307-AD95-42FA-8234-C551BDB7BFA1');
INSERT INTO `security_role_auth` VALUES ('822DB257-EB1B-4EDC-BA47-E9414261CD93', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'CBD4685F-AC93-4574-95CB-BFF7E900BD2E');
INSERT INTO `security_role_auth` VALUES ('8237F411-686A-4BEC-98B2-FF1EC4B9FA34', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'D413878A-2092-40D8-B946-1B6F90D2F194');
INSERT INTO `security_role_auth` VALUES ('825059F0-BBD0-4706-A43C-1B0B0F67D376', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '82384F1D-4CD1-4DF3-9121-B80EE0B15973');
INSERT INTO `security_role_auth` VALUES ('82C6412C-D275-4BCE-844A-DD3BEF738ECE', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '28DFDD64-D05F-4D87-A997-11F29FA27042');
INSERT INTO `security_role_auth` VALUES ('85BEC33E-4A68-4B4A-9DA7-19248667FB4A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '3F844D55-E2AA-43C0-B795-88B5DE2440CF');
INSERT INTO `security_role_auth` VALUES ('87021316-B1B8-429B-A323-5012B685AC92', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '7FFE281D-6135-4180-84DD-8C2715E7EDC7');
INSERT INTO `security_role_auth` VALUES ('8875732E-86C0-41A2-BD3D-BF02E677296C', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '01E6A560-0552-46BC-8A5F-0C2BEB987617');
INSERT INTO `security_role_auth` VALUES ('8954FA9E-E173-4F27-9C13-B7AC074F3D90', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '00042A5A-FF5C-4EF2-89BB-5A6CB2D7EDA6');
INSERT INTO `security_role_auth` VALUES ('897650B4-5F12-4180-99F3-8104A4A57560', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('89AB9B7E-C1E9-4F04-831B-772B784A6B84', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('89EF9C21-A23D-4CAD-B7C7-75A4C1EAF877', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Handler', '2287114B-17C2-4E88-8A2F-505709ADB719');
INSERT INTO `security_role_auth` VALUES ('8B5F57D1-1B73-4952-908F-A4AB935FFF8C', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '4E2CAF22-3DCB-415D-ABC5-0E6BC2B50BFE');
INSERT INTO `security_role_auth` VALUES ('8BCC713E-5461-4027-B43E-015E1E1E33D5', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '01E6A560-0552-46BC-8A5F-0C2BEB987617');
INSERT INTO `security_role_auth` VALUES ('8C75CD55-33C0-4A6A-8FDD-8F4F232C999A', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'FCD061B9-45BC-4E7A-86CB-B179E30B0A76');
INSERT INTO `security_role_auth` VALUES ('8E46B74F-9682-46FE-BA9D-E5645762EEE9', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `security_role_auth` VALUES ('900433C2-2907-4C20-BA9A-BA1D53AAE79B', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '61DF22C0-98A7-4D4B-8B7B-B6A29FCB375F');
INSERT INTO `security_role_auth` VALUES ('90A12035-FFBE-42A3-A5E9-40096777A419', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'A2334908-E3F6-4896-B209-5856B541BC82');
INSERT INTO `security_role_auth` VALUES ('91F0E60A-B638-46EB-809B-5BCA9CAD6718', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '29596313-EBFD-4D56-A72D-16A91F6FA795');
INSERT INTO `security_role_auth` VALUES ('929537C6-00D1-4DD5-A577-3C25445ACD13', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '8EF8E532-1B93-440D-B0D6-680D7F72207B');
INSERT INTO `security_role_auth` VALUES ('929FFFE5-7F14-42DE-AAF7-311D50BD2B8A', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'BACCCF29-7D2E-4C49-A03A-F1F63DDBBA15');
INSERT INTO `security_role_auth` VALUES ('92E97977-3DE6-4CFA-A2BD-A8034B103852', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Menu', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `security_role_auth` VALUES ('963687E8-813D-40C0-BECE-17DE3314947C', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('977AE16D-C9B8-495B-8592-5B50458E8F65', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1BBAA470-C306-4915-9ECC-EBCF72EDC38D');
INSERT INTO `security_role_auth` VALUES ('97D25C67-2427-4DDA-BA16-5B0690C7D46E', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', '90FCC64A-FFAD-4E8B-BBAE-EA0AE4C27CB9');
INSERT INTO `security_role_auth` VALUES ('9801A5B0-8FB9-42F3-B3C1-A7BC4FBA7E80', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('984D8A36-57B4-4E48-BAB9-85519E96A7F9', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', 'BACCCF29-7D2E-4C49-A03A-F1F63DDBBA15');
INSERT INTO `security_role_auth` VALUES ('98867753-357B-477F-9C54-4D6F9FB6DF26', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '5EEFEF32-70DB-4763-857F-7935E418E570');
INSERT INTO `security_role_auth` VALUES ('9988A21F-A097-4C50-B99C-F78FCCC97208', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'EC67A415-CC8F-45B5-A28D-797D84435841');
INSERT INTO `security_role_auth` VALUES ('99E46F77-16A0-4D6F-AAA3-5EF2EB96B39F', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'CEA130F1-3BD5-47D9-84E5-2B2003D8F20B');
INSERT INTO `security_role_auth` VALUES ('9B268D2D-726B-4747-A02B-E108D05C4E67', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '894F3E00-FF69-494E-A072-5D63D185097E');
INSERT INTO `security_role_auth` VALUES ('A05F9B00-943D-4DD1-8F21-E410BF1D01DA', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('A07DCA35-5485-4832-9BD2-1D91B59BAD07', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'F4C8FD10-7ED1-4912-B7A5-92DFA7070159');
INSERT INTO `security_role_auth` VALUES ('A1854BAC-8B25-4F21-8B50-81B5968EA98A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '2BF95B4C-B823-402B-AB8C-284CE7EDE678');
INSERT INTO `security_role_auth` VALUES ('A2679AC8-4B8E-464E-BBB9-A57B61CB8F77', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Menu', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `security_role_auth` VALUES ('A3C0A870-A52D-4CA6-AF27-9799EC742E02', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA');
INSERT INTO `security_role_auth` VALUES ('A3C3D8C6-FF38-4207-819C-94AAF2668AAD', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `security_role_auth` VALUES ('A54BD53C-600F-4286-B214-5DBF6550B6C0', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '35067AE0-9243-450A-B9D7-59B550CDD8D2');
INSERT INTO `security_role_auth` VALUES ('A5D89A39-294C-4FE1-AE35-285C8E688553', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '579FCABC-B5D5-4157-91DD-D638943C3BCF');
INSERT INTO `security_role_auth` VALUES ('A70B0AC5-63CF-4B30-A23E-A00DAB2ACF9E', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', 'B5CD6899-DECC-450D-A69E-BF82B0A5DAD9');
INSERT INTO `security_role_auth` VALUES ('A772121C-5BD0-42F1-BB55-C2BADADE2A7F', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'D28C6156-32D2-44C4-90D4-63DC05CD6407');
INSERT INTO `security_role_auth` VALUES ('A976AB64-36BD-45D6-95A6-10D2B4028AE4', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8');
INSERT INTO `security_role_auth` VALUES ('A97CD615-1987-4D5E-A835-26D26D1BD519', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'CC288486-F786-4815-8BFB-A6C1374A4BCB');
INSERT INTO `security_role_auth` VALUES ('A9BE05E4-A838-46D7-AA2F-C7A9C57A34D9', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '35067AE0-9243-450A-B9D7-59B550CDD8D2');
INSERT INTO `security_role_auth` VALUES ('A9F0E7CB-DB98-4D23-9DDC-4189509C94FA', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '2A03F130-73A4-4A11-A03A-121BE4B1BFA0');
INSERT INTO `security_role_auth` VALUES ('AA1B2926-68A7-499B-A035-109832F8411F', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '02E89343-F996-44EC-8347-6FB3C60BC9BB');
INSERT INTO `security_role_auth` VALUES ('AD0C6CCD-FD07-4B99-A067-C10FD3F12A92', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');
INSERT INTO `security_role_auth` VALUES ('B08B69CF-4B17-4538-9631-B037AB11F2BF', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '2287114B-17C2-4E88-8A2F-505709ADB719');
INSERT INTO `security_role_auth` VALUES ('B1A475B6-F7C0-4079-9015-509E21780600', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'A3D11759-6C21-4B36-9BA2-2CE232C39085');
INSERT INTO `security_role_auth` VALUES ('B2DBE929-7E81-4BBC-BAC1-7106DEE2826F', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '6C16A2E5-0128-4D93-84C3-E9BB9F2327B6');
INSERT INTO `security_role_auth` VALUES ('B369A769-D39A-42B6-8A08-9E36F801BADB', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('B62EF221-3E30-4CA6-A4A9-72AC4045D9FF', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', 'AA10D06A-C8FB-4C1F-8193-A81F1BCD438B');
INSERT INTO `security_role_auth` VALUES ('B763774A-2D25-46E4-BCA7-79161665DCBF', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Handler', '12691874-9834-440C-929A-87C699B7AA4E');
INSERT INTO `security_role_auth` VALUES ('B79E3C43-2ED4-49EF-A666-855335A796C6', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '1B18D08B-2066-4259-A9AE-4987FB58C3E4');
INSERT INTO `security_role_auth` VALUES ('B7EDEFEF-7DF1-46DA-93A8-562A3277C4C8', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '1CD98EC9-334B-40EE-8AE4-CDDAB4E99B57');
INSERT INTO `security_role_auth` VALUES ('B8053BCF-C268-41E7-9C2E-9885ADE35017', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '2321E4A1-2CB5-430C-8841-5129D02D2C38');
INSERT INTO `security_role_auth` VALUES ('B83B96EC-B0D1-4900-B162-10381FEA86DC', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'FEE4BDAE-A1ED-47E1-9065-22285EB8F57B');
INSERT INTO `security_role_auth` VALUES ('B853E4EC-5B19-4877-BBC0-3A08135E8D04', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('B989A14F-6766-4F94-AC99-490F5FEBBA4A', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'C5D919FA-A812-4AB2-AED5-81BE5B31B79D');
INSERT INTO `security_role_auth` VALUES ('BA619135-BC6F-4F61-BF89-CDE0B3FE5ABA', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'CEA130F1-3BD5-47D9-84E5-2B2003D8F20B');
INSERT INTO `security_role_auth` VALUES ('BA7101C5-319C-4B75-A50D-3BD2B99AD1B7', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA');
INSERT INTO `security_role_auth` VALUES ('BAE0CC5D-3D63-4061-8A47-B1360F4FECA3', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '2A03F130-73A4-4A11-A03A-121BE4B1BFA0');
INSERT INTO `security_role_auth` VALUES ('BB2750B7-71F8-434E-BFFA-0E2B3D2D8E94', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '88F99F91-384F-44B1-9EC8-E05F6724A9E8');
INSERT INTO `security_role_auth` VALUES ('BC9603EA-5A97-4B60-A706-0C4B11857637', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '33CF8E06-1181-453B-ADF4-1F18ABC026CF');
INSERT INTO `security_role_auth` VALUES ('BCDE4A5D-0DBA-4BE4-8352-7238459D9B3F', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'AEA07454-68B1-453D-ABC3-C6CD10026CFA');
INSERT INTO `security_role_auth` VALUES ('BD537B9B-DAD1-4EC2-BB1F-909E6943B384', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'FCD061B9-45BC-4E7A-86CB-B179E30B0A76');
INSERT INTO `security_role_auth` VALUES ('BD84B0E1-B953-446A-8521-31D8B5F7FC7C', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '3B1CB4FE-03E4-406B-99FA-B17520AA9C1E');
INSERT INTO `security_role_auth` VALUES ('BF162BA2-5014-4AAB-8CFB-6693F67F5884', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '76FF0E56-DA31-4399-82BF-8B1C9B253872');
INSERT INTO `security_role_auth` VALUES ('BFEE4DC3-8B5A-4B5E-ACAD-79F70D3D625A', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'B8022643-0966-4493-84BA-23A77B09AD2C');
INSERT INTO `security_role_auth` VALUES ('C009AE61-8907-4E31-9F0B-FB7C3503D681', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Menu', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `security_role_auth` VALUES ('C05C3CCE-4761-4240-AB73-FDA62FB62635', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'CF4DB6EE-7AD3-481D-90C0-125CA6329E4C');
INSERT INTO `security_role_auth` VALUES ('C0D4842D-416B-4600-8B5D-66E4B81B57D0', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '6224A19C-0FC4-439F-B5C8-79E86557B29E');
INSERT INTO `security_role_auth` VALUES ('C0F2FFD6-457E-42C1-AE6E-E048B7217313', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '24A26858-4628-472E-810B-964E817E7176');
INSERT INTO `security_role_auth` VALUES ('C120597A-7B2F-4F72-AFC0-64CD76A936DA', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '567FBEE1-306B-4EC1-8688-906D5629C3D4');
INSERT INTO `security_role_auth` VALUES ('C38A5E1A-958D-45B7-9760-D8215833D566', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '199DCC87-DB0D-4F78-A0AE-CCE348B13516');
INSERT INTO `security_role_auth` VALUES ('C409E11F-26C8-4ADC-956A-C9F1B1508AEF', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', 'FE700723-9D3F-46D8-9C02-949AF8F9B939');
INSERT INTO `security_role_auth` VALUES ('C4286182-7FD7-493F-A3C5-7025FB91D6E1', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '1B18D08B-2066-4259-A9AE-4987FB58C3E4');
INSERT INTO `security_role_auth` VALUES ('C4C478E6-9875-4972-B3D4-5A2F6D700668', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'A2334908-E3F6-4896-B209-5856B541BC82');
INSERT INTO `security_role_auth` VALUES ('C541BFCC-6AA5-4458-8124-522A0BEFDBD8', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '6224A19C-0FC4-439F-B5C8-79E86557B29E');
INSERT INTO `security_role_auth` VALUES ('CB9B566A-0999-4D7E-BB03-08B2542756A3', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('CBB53DAF-FF3E-4DC2-BE7A-842EBF9E2434', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Handler', 'B4790CBE-FB9F-4D2C-8243-7400AF03F1CA');
INSERT INTO `security_role_auth` VALUES ('CC5DBBB4-E5C6-4F86-9D76-358864816A4F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '3743767A-8D65-4466-B0A3-D2C11F07B5EC');
INSERT INTO `security_role_auth` VALUES ('CCDE498C-E22F-4E64-937D-F683C0750405', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '90FCC64A-FFAD-4E8B-BBAE-EA0AE4C27CB9');
INSERT INTO `security_role_auth` VALUES ('CD45E39E-9E15-4A00-9549-7348C177A3A9', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '24A26858-4628-472E-810B-964E817E7176');
INSERT INTO `security_role_auth` VALUES ('CD4CC7B6-F0E5-40CE-884D-33A4A4B0A747', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '4A64B8DC-7370-435D-8F10-612EC45FFCD8');
INSERT INTO `security_role_auth` VALUES ('CE5EF8D9-55A8-4605-B5B4-3A2D810566DD', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '910D3B2D-0019-4FAD-8593-43710B416A3B');
INSERT INTO `security_role_auth` VALUES ('CEC6A18A-D3E1-498A-AF07-BAADD5F40887', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('CEFE1277-4412-42B5-AA92-D37592EC1B66', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '61DF22C0-98A7-4D4B-8B7B-B6A29FCB375F');
INSERT INTO `security_role_auth` VALUES ('D27AF497-A948-46F8-8BDF-BDC79CA323E3', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'AEA07454-68B1-453D-ABC3-C6CD10026CFA');
INSERT INTO `security_role_auth` VALUES ('D2C4AC31-5B32-418B-B388-498C3C77AAFE', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('D424F346-9D89-429E-8E61-3D765D76E696', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Operation', 'CF4DB6EE-7AD3-481D-90C0-125CA6329E4C');
INSERT INTO `security_role_auth` VALUES ('D44C64FC-F54A-4850-95EE-1ECE7CE4EF9C', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '7683F231-6534-4ADA-863E-F7637017351A');
INSERT INTO `security_role_auth` VALUES ('D568A397-933B-4A0A-81BD-120354A50D50', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('D58F2BB0-C709-421C-99DB-30C393AD1C54', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '1DC1E347-7C48-4BE8-A271-9FDCE5A074BB');
INSERT INTO `security_role_auth` VALUES ('D5FC5C5A-EC92-408D-BCEE-70005535D992', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '790FA246-8461-4607-A046-903A2C8DE118');
INSERT INTO `security_role_auth` VALUES ('D644596A-D0E0-4C78-8499-470B8797C933', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '7211605C-F7D5-4C45-8918-B3FC938F5EF5');
INSERT INTO `security_role_auth` VALUES ('D6A60A78-6118-476F-9E84-98DF72D4C606', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344');
INSERT INTO `security_role_auth` VALUES ('D701C5AE-167F-40D2-8D93-37A1E2CD6EFF', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '31F4B92B-FEB0-46FC-828E-ECB572AEC860');
INSERT INTO `security_role_auth` VALUES ('D879A022-E55D-4FDB-BF66-9CEC59D9EF3F', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'FFDB914C-B3F6-4871-8AEE-515A1081ACF0');
INSERT INTO `security_role_auth` VALUES ('D9611240-7738-4035-AC4E-D9A46BB178D4', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '1BF7D254-304B-4719-BCB6-AB38BF8CA4E7');
INSERT INTO `security_role_auth` VALUES ('D969CC85-55FB-43E2-9D4E-3F1A9AE7DD01', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'B5CD6899-DECC-450D-A69E-BF82B0A5DAD9');
INSERT INTO `security_role_auth` VALUES ('D98FE66E-7228-441F-96AF-7E25C5C74701', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'D28C6156-32D2-44C4-90D4-63DC05CD6407');
INSERT INTO `security_role_auth` VALUES ('D9ED2AFC-5FAD-4DF9-983E-C46D7DE0D464', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '76FF0E56-DA31-4399-82BF-8B1C9B253872');
INSERT INTO `security_role_auth` VALUES ('DA102F34-1381-4723-BBB7-C4E595CC8481', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '7211605C-F7D5-4C45-8918-B3FC938F5EF5');
INSERT INTO `security_role_auth` VALUES ('DABAE3DA-E548-49B0-B9BF-B547C3486329', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Handler', '2287114B-17C2-4E88-8A2F-505709ADB719');
INSERT INTO `security_role_auth` VALUES ('DAC84D4F-D803-466D-9B10-5ADA9FDD8954', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '2BF95B4C-B823-402B-AB8C-284CE7EDE678');
INSERT INTO `security_role_auth` VALUES ('DB264284-D24E-4611-8DA7-55BAD19203F2', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '30E6EF99-55BD-4B4F-82E1-D2E4344D8ABD');
INSERT INTO `security_role_auth` VALUES ('DBA12AD2-64B9-4440-AE9C-A599BF95F7D6', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Menu', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `security_role_auth` VALUES ('DC4D13C8-CE94-4C9B-A9C7-71F7BB004A0D', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '863E15B1-7DF1-4A73-8F19-1F9CD0B7EFAB');
INSERT INTO `security_role_auth` VALUES ('DCF522CE-EA4F-4637-98DE-0F2983CB151D', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '093413BA-1ABC-464F-AEA0-13FF82591D22');
INSERT INTO `security_role_auth` VALUES ('DD1928B7-62C8-4D02-A8FA-63A16C873741', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '0831D3B9-7BD5-4581-A540-72982B899171');
INSERT INTO `security_role_auth` VALUES ('DD7C7FDA-4ADE-4E4C-AB42-E25ED2679093', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '2287114B-17C2-4E88-8A2F-505709ADB719');
INSERT INTO `security_role_auth` VALUES ('DE2DBD9F-7DE0-4C4C-853B-07A255779174', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '34D36130-6300-425B-8820-E05C1EF2A2E6');
INSERT INTO `security_role_auth` VALUES ('DF5B72E1-716B-43FE-A485-752D68CBEEFB', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '4AE7E5CB-848D-483C-A59A-BCD1117A7356');
INSERT INTO `security_role_auth` VALUES ('DFFEF266-5B18-4DE8-817D-C8B7E57CE4BF', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '8EF8E532-1B93-440D-B0D6-680D7F72207B');
INSERT INTO `security_role_auth` VALUES ('E0B4FCA8-2852-488C-B97A-F2E991CDD748', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'DCC185BC-660A-4172-A5D4-C2B40EE0833D');
INSERT INTO `security_role_auth` VALUES ('E10034CC-8211-4429-AA5F-C317104EE98E', '6A79C287-E013-4A92-AAB2-B926F38144BB', 'Handler', '76FF0E56-DA31-4399-82BF-8B1C9B253872');
INSERT INTO `security_role_auth` VALUES ('E195D9D7-C256-401A-B1B1-2006F2969054', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', 'E97EEF76-3B5F-4863-B8B8-FA4FBC76E0B8');
INSERT INTO `security_role_auth` VALUES ('E1B8DB41-BFBE-40AD-97AC-1EA63CA41B41', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('E283AC11-BD0D-4207-BC62-CBC8DF24D75B', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'DCC185BC-660A-4172-A5D4-C2B40EE0833D');
INSERT INTO `security_role_auth` VALUES ('E2FC726D-A57F-4315-8371-AA3B41E588BE', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '1DC1E347-7C48-4BE8-A271-9FDCE5A074BB');
INSERT INTO `security_role_auth` VALUES ('E30A4AAA-AA43-4C34-AE4A-9112C48C878C', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '567FBEE1-306B-4EC1-8688-906D5629C3D4');
INSERT INTO `security_role_auth` VALUES ('E31B5A66-6F58-4553-B1C9-0BE5C7A0F948', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Menu', '0BC027DB-9D9C-4A69-9318-384966FA6CFE');
INSERT INTO `security_role_auth` VALUES ('E38F916D-88AC-4423-B269-55F611DDCD76', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D');
INSERT INTO `security_role_auth` VALUES ('E4EAED86-8AFD-4690-844C-1C912B12F097', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '3743767A-8D65-4466-B0A3-D2C11F07B5EC');
INSERT INTO `security_role_auth` VALUES ('E627ABF2-6D97-45E0-964E-1FDC6BD28FE3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'D57A45A8-FD4C-4081-A5C8-BBC7E5DD198D');
INSERT INTO `security_role_auth` VALUES ('E66AD662-9015-4ECA-B4DA-ED398FE1C6E0', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `security_role_auth` VALUES ('E6D496E3-B3B0-45D4-8526-F168723568B3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '29596313-EBFD-4D56-A72D-16A91F6FA795');
INSERT INTO `security_role_auth` VALUES ('E6DE1055-5FA8-4C12-8F79-F70EA9305330', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('E7E0F21C-EEDB-46B6-B795-22A427A08402', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Menu', '029FB2F2-370F-46D5-A283-A22C8E49341C');
INSERT INTO `security_role_auth` VALUES ('E90043CA-556D-4580-83E6-FB0A201009F6', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `security_role_auth` VALUES ('EB1CB2FA-24EE-4DE2-B65D-E1B340493E40', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '2CAE2167-AA7B-437D-8953-C0E392B15AFC');
INSERT INTO `security_role_auth` VALUES ('EB64FC24-B202-4CB6-8482-E4AC5E1DBD36', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Operation', '6C16A2E5-0128-4D93-84C3-E9BB9F2327B6');
INSERT INTO `security_role_auth` VALUES ('EC76BE3A-76B6-46E0-958D-93EDCCC9F724', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '863E15B1-7DF1-4A73-8F19-1F9CD0B7EFAB');
INSERT INTO `security_role_auth` VALUES ('EC7A485B-EC7A-4481-9A6E-43A339CDDD36', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', 'A2334908-E3F6-4896-B209-5856B541BC82');
INSERT INTO `security_role_auth` VALUES ('ED9718D2-5F5B-4C96-8A87-703A259FE9BD', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO `security_role_auth` VALUES ('EE384CF9-BE58-4469-86BE-499069694903', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Handler', '12691874-9834-440C-929A-87C699B7AA4E');
INSERT INTO `security_role_auth` VALUES ('EF6FF974-D228-4B56-9901-12B42E952412', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Handler', '3890BD46-F782-4036-9FC3-E6DA46140C5D');
INSERT INTO `security_role_auth` VALUES ('EF7AFB40-FDDF-4599-858A-318F59C69CF9', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '1B18D08B-2066-4259-A9AE-4987FB58C3E4');
INSERT INTO `security_role_auth` VALUES ('F023066B-1BC2-4970-9F23-6153BEFAE801', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '61DF22C0-98A7-4D4B-8B7B-B6A29FCB375F');
INSERT INTO `security_role_auth` VALUES ('F0DAEB63-4438-466F-A82E-FF0D2BB7828F', '05FC4E42-2BCF-4651-BDFF-86444D055695', 'Operation', '6D3B756C-739B-4075-A69C-750F539223B3');
INSERT INTO `security_role_auth` VALUES ('F0DBFC70-EB77-486E-B5F1-D8AD29BCC6A1', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '5524CB68-469B-4163-99EF-1EDDE8E13AFD');
INSERT INTO `security_role_auth` VALUES ('F0E6FCAE-451C-45AD-ACA8-F748F97395D5', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '34D36130-6300-425B-8820-E05C1EF2A2E6');
INSERT INTO `security_role_auth` VALUES ('F2F831E8-5E1E-4F15-836A-891572927C21', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '6C24D134-9C82-4516-BBBD-1EDB00CBA951');
INSERT INTO `security_role_auth` VALUES ('F6002DA3-E900-422D-94DE-1909F3CA78B2', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', 'Handler', '3743767A-8D65-4466-B0A3-D2C11F07B5EC');
INSERT INTO `security_role_auth` VALUES ('F81CE2E1-247D-4A26-A3E9-D639A3EB26C3', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Operation', '0A92F33C-5E91-4401-99C2-1FB41C8BE0B9');
INSERT INTO `security_role_auth` VALUES ('F8525553-DF6C-43CA-BA6E-D02B9DDDECEA', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', 'A2334908-E3F6-4896-B209-5856B541BC82');
INSERT INTO `security_role_auth` VALUES ('F98E6C7B-7D5C-4450-879B-22B8157B3FE6', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '9E812AE0-CFE4-4891-B7BF-22A22B643543');
INSERT INTO `security_role_auth` VALUES ('F9C50CAF-4D58-4E9B-96D1-DAEAFE61736D', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'AEEDFDAB-7802-492D-B369-2DEE350208B5');
INSERT INTO `security_role_auth` VALUES ('F9FE2568-93FE-455A-88FB-346CEF9265DD', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', 'DCC185BC-660A-4172-A5D4-C2B40EE0833D');
INSERT INTO `security_role_auth` VALUES ('FB2AC7A9-37C5-47C9-B32E-F0582873D179', '0A6C302D-8531-4C7D-AF38-B7B653CBA668', 'Operation', '1DC1E347-7C48-4BE8-A271-9FDCE5A074BB');
INSERT INTO `security_role_auth` VALUES ('FD2CB60B-F4A0-424C-86FB-7900E757CFA2', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Operation', '4AE7E5CB-848D-483C-A59A-BCD1117A7356');
INSERT INTO `security_role_auth` VALUES ('FE4E7AFD-4DE2-40B9-8BD9-63F0924FBCF4', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO `security_role_auth` VALUES ('FECD25CF-2A22-46E6-B2D4-C6EFD33FB851', 'DDA88E11-231C-43EC-9657-7AA55D842407', 'Handler', '1DA91DE7-BE74-4DB7-BC3A-5E384BC6D344');

-- ----------------------------
-- Table structure for security_role_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_role_group_rel`;
CREATE TABLE `security_role_group_rel` (
  `GRP_ID` varchar(36) NOT NULL,
  `ROLE_ID` varchar(36) NOT NULL,
  `RG_ID` char(36) NOT NULL,
  PRIMARY KEY (`RG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_role_group_rel
-- ----------------------------
INSERT INTO `security_role_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '9C7CFCED-8479-43B6-B2FE-5CAA663C2D51', '0F21899D-D079-4971-BC50-017E1E68EC2C');
INSERT INTO `security_role_group_rel` VALUES ('23F5915B-C8F1-4BA5-AED9-7CE50B11D5F4', '25788B54-CE0A-4137-8890-EFA4F0DE06B6', '2A05970E-F3EC-4C97-A0F5-1DCEE6A4243D');
INSERT INTO `security_role_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '05FC4E42-2BCF-4651-BDFF-86444D055695', '4AD66E62-DB42-4357-AE64-952210079464');
INSERT INTO `security_role_group_rel` VALUES ('21E50836-912E-4DB8-82CB-9B32C8A44C9F', '00000000-0000-0000-00000000000000000', '5F8643A7-8A1B-4BA0-B6EB-6B20EA916D97');
INSERT INTO `security_role_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'DDA88E11-231C-43EC-9657-7AA55D842407', '749A4591-CCF7-4783-95BD-28C73C5062C3');
INSERT INTO `security_role_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '00000000-0000-0000-00000000000000000', '7EFF00F6-491B-4958-A828-239A2C85D4EE');
INSERT INTO `security_role_group_rel` VALUES ('23F5915B-C8F1-4BA5-AED9-7CE50B11D5F4', '00000000-0000-0000-00000000000000000', '8DBE309A-C820-4704-ADBF-564ACFD1FE9D');
INSERT INTO `security_role_group_rel` VALUES ('BBD420A2-68AE-49C2-B3D8-78DC166F511F', '00000000-0000-0000-00000000000000000', 'B1789F7C-C87F-4069-927F-96339E9B279C');
INSERT INTO `security_role_group_rel` VALUES ('21E50836-912E-4DB8-82CB-9B32C8A44C9F', '25788B54-CE0A-4137-8890-EFA4F0DE06B6', 'C310A8D9-53DF-4482-815B-DB1876997B33');
INSERT INTO `security_role_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '8AAF1DCF-114D-49D4-B691-A46F77288AF0', 'C39BECE8-47E3-49BC-99ED-3090CEA24009');
INSERT INTO `security_role_group_rel` VALUES ('315F898C-A008-4F77-BAB0-4FDF935F7B1F', '00000000-0000-0000-00000000000000000', 'CF5B7EF2-4044-4B31-A934-2BDD8D356FAE');
INSERT INTO `security_role_group_rel` VALUES ('315F898C-A008-4F77-BAB0-4FDF935F7B1F', '25788B54-CE0A-4137-8890-EFA4F0DE06B6', 'E435CFD5-1360-409D-BA19-6563A25AB241');

-- ----------------------------
-- Table structure for security_user
-- ----------------------------
DROP TABLE IF EXISTS `security_user`;
CREATE TABLE `security_user` (
  `USER_ID` varchar(36) NOT NULL,
  `USER_CODE` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `USER_PWD` varchar(32) DEFAULT NULL,
  `USER_SEX` varchar(1) DEFAULT NULL,
  `USER_DESC` varchar(128) DEFAULT NULL,
  `USER_STATE` varchar(32) DEFAULT NULL,
  `USER_SORT` int(11) DEFAULT NULL,
  `USER_MAIL` varchar(64) DEFAULT NULL,
  `USER_PHONE` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user
-- ----------------------------
INSERT INTO `security_user` VALUES ('2CE11AC8-0E03-488F-BF7C-9568DFDD9C7A', 'CS04', '赵小四', 'FBAF749E6A085DCBBE41FF5C030EBF98', 'M', '', '1', '4', '', '');
INSERT INTO `security_user` VALUES ('439D83A6-148E-483E-BF6C-9EF131C27ACF', 'CS01', '张老大', 'DAE6CDC2B49F7C32164BE2AF1A7916AA', 'M', '', '1', '1', '', '');
INSERT INTO `security_user` VALUES ('542798C0-9D66-4DF2-B5B1-70D9323B954F', 'CS03', '孙小三', '25EC07853BDC7B87DD021F5580C70855', 'M', '', '1', '3', '', '');
INSERT INTO `security_user` VALUES ('7ACD7AD5-9A6F-4616-8122-A17D42288BE1', 'userr', 'user', 'EE11CBB19052E40B07AAC0CA060C23EE', 'M', '', '1', '1', '', '');
INSERT INTO `security_user` VALUES ('7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'admin', '管理员', '21232F297A57A5A743894A0E4A801FC3', 'M', '内置账户，勿删！！', '1', '1', null, null);
INSERT INTO `security_user` VALUES ('B587A80F-1863-434C-999F-A92C240BD314', 'CS02', '赵晓二', '39661AF3C6AFE19E95700A0E7373446A', 'F', '', '1', '2', '', '');

-- ----------------------------
-- Table structure for security_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `security_user_auth`;
CREATE TABLE `security_user_auth` (
  `USER_AUTH_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `RES_TYPE` varchar(32) DEFAULT NULL,
  `RES_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`USER_AUTH_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_auth
-- ----------------------------

-- ----------------------------
-- Table structure for security_user_group_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_group_rel`;
CREATE TABLE `security_user_group_rel` (
  `GRP_ID` varchar(36) NOT NULL,
  `USER_ID` varchar(36) NOT NULL,
  `GU_ID` char(36) NOT NULL,
  PRIMARY KEY (`GU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_group_rel
-- ----------------------------
INSERT INTO `security_user_group_rel` VALUES ('00000000-0000-0000-00000000000000000', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', '0A989782-15CF-4C2F-960D-B9BABCD6C6FD');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'B587A80F-1863-434C-999F-A92C240BD314', '1D891D40-82FF-4D2E-B570-DC3DE18B2F17');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', '32738BEE-BBA9-49E8-9D68-49CA6438B952');
INSERT INTO `security_user_group_rel` VALUES ('0BE47DDB-862E-45F3-8E43-0A4990847D8B', '7ACD7AD5-9A6F-4616-8122-A17D42288BE1', '7B68E56D-3B08-43FF-9DBD-AED1E7FF5B3D');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '439D83A6-148E-483E-BF6C-9EF131C27ACF', '9AB45B8C-CD35-4CE0-8882-482E1CFC6C17');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '2CE11AC8-0E03-488F-BF7C-9568DFDD9C7A', 'C59A348D-4591-4637-9652-8CDFE0D0C2B8');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '542798C0-9D66-4DF2-B5B1-70D9323B954F', 'CFCB635D-1C53-4D62-B78A-9EFA86D823AD');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '7ACD7AD5-9A6F-4616-8122-A17D42288BE1', 'D8C5AF49-A437-414B-B35D-07128A2D3EA7');
INSERT INTO `security_user_group_rel` VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '9A0A9DE7-608A-4B2B-B1C7-F6C11FD1A94E', 'DE4A0F9E-EEFE-40AE-B90B-5E51143242A3');

-- ----------------------------
-- Table structure for security_user_rg_rel
-- ----------------------------
DROP TABLE IF EXISTS `security_user_rg_rel`;
CREATE TABLE `security_user_rg_rel` (
  `URG_ID` char(36) NOT NULL,
  `USER_ID` varchar(36) DEFAULT NULL,
  `RG_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`URG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of security_user_rg_rel
-- ----------------------------
INSERT INTO `security_user_rg_rel` VALUES ('116C92F6-5172-4890-B0C5-15F808F07821', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', '7EFF00F6-491B-4958-A828-239A2C85D4EE');
INSERT INTO `security_user_rg_rel` VALUES ('24E39E4B-A2FB-41DE-9C90-998E19F8B31C', '542798C0-9D66-4DF2-B5B1-70D9323B954F', '0F21899D-D079-4971-BC50-017E1E68EC2C');
INSERT INTO `security_user_rg_rel` VALUES ('3966A72C-014C-42F0-8D41-179D384F893B', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'CF5B7EF2-4044-4B31-A934-2BDD8D356FAE');
INSERT INTO `security_user_rg_rel` VALUES ('3A7B224D-BCEF-4D6E-9A1D-9E6691C23B3F', 'B587A80F-1863-434C-999F-A92C240BD314', '4AD66E62-DB42-4357-AE64-952210079464');
INSERT INTO `security_user_rg_rel` VALUES ('3C3CD0FB-118B-43AC-BEFB-E58C144A6BAD', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', '8DBE309A-C820-4704-ADBF-564ACFD1FE9D');
INSERT INTO `security_user_rg_rel` VALUES ('3C4180F5-78B8-4CE3-BFBC-A39FFAD427C5', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'CF5B7EF2-4044-4B31-A934-2BDD8D356FAE');
INSERT INTO `security_user_rg_rel` VALUES ('404D6E41-BB2D-42D4-91DD-4FAA4F64C709', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'B1789F7C-C87F-4069-927F-96339E9B279C');
INSERT INTO `security_user_rg_rel` VALUES ('445ADEC7-3790-4B9F-8E92-21BFDD922253', '2CE11AC8-0E03-488F-BF7C-9568DFDD9C7A', 'C39BECE8-47E3-49BC-99ED-3090CEA24009');
INSERT INTO `security_user_rg_rel` VALUES ('46D45BB6-46BB-42D3-A3C4-7A73D11ACB93', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', '7EFF00F6-491B-4958-A828-239A2C85D4EE');
INSERT INTO `security_user_rg_rel` VALUES ('4F33F6E1-FADD-4300-AD0B-11F6179E6D05', '439D83A6-148E-483E-BF6C-9EF131C27ACF', '749A4591-CCF7-4783-95BD-28C73C5062C3');
INSERT INTO `security_user_rg_rel` VALUES ('60D7BEC6-9EB3-40DD-BB80-F39BCEED0041', 'B587A80F-1863-434C-999F-A92C240BD314', 'C2E9C5EC-8176-4B85-9AF6-7EBFA87AD573');
INSERT INTO `security_user_rg_rel` VALUES ('67168533-02DB-400E-B0F4-B741AD3A5BCA', '2CE11AC8-0E03-488F-BF7C-9568DFDD9C7A', '2BB576EF-5471-40B0-BC08-5565961CF954');
INSERT INTO `security_user_rg_rel` VALUES ('88C5DBD2-2348-4D89-91CD-8F8E7D806BFA', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', '5F8643A7-8A1B-4BA0-B6EB-6B20EA916D97');
INSERT INTO `security_user_rg_rel` VALUES ('8D87EE5C-A0B2-402C-AAD1-2B460BBB9126', '439D83A6-148E-483E-BF6C-9EF131C27ACF', '20618096-71D5-4A7B-B745-4AC0DCEB3950');
INSERT INTO `security_user_rg_rel` VALUES ('99098D3A-1D02-46C3-8B19-68DD80B9F75D', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', '5F8643A7-8A1B-4BA0-B6EB-6B20EA916D97');
INSERT INTO `security_user_rg_rel` VALUES ('A83F36E8-A0D6-427D-9A51-6FE23884CC22', '542798C0-9D66-4DF2-B5B1-70D9323B954F', 'A1F3C828-0256-4D0C-AC95-589C58B4032E');
INSERT INTO `security_user_rg_rel` VALUES ('D7EB3056-AD06-4A05-AB76-C251CFA019A8', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', '8DBE309A-C820-4704-ADBF-564ACFD1FE9D');
INSERT INTO `security_user_rg_rel` VALUES ('EE56AE91-DF2E-4881-A15C-45236A3D8F28', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'B1789F7C-C87F-4069-927F-96339E9B279C');

-- ----------------------------
-- Table structure for sys_codelist
-- ----------------------------
DROP TABLE IF EXISTS `sys_codelist`;
CREATE TABLE `sys_codelist` (
  `TYPE_ID` varchar(32) NOT NULL,
  `CODE_ID` varchar(32) NOT NULL,
  `CODE_NAME` varchar(32) DEFAULT NULL,
  `CODE_DESC` varchar(128) DEFAULT NULL,
  `CODE_SORT` int(11) DEFAULT NULL,
  `CODE_FLAG` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`,`CODE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codelist
-- ----------------------------
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Bottom', 'BottomHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Building', 'BuildingHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Homepage', 'HomepageHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Logo', 'LogoHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MainWin', 'MainWinHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'MenuTree', 'MenuTreeHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('AuthedHandlerId', 'Navigater', 'NavigaterHandler', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('BOOL_DEFINE', 'N', '否', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('BOOL_DEFINE', 'Y', '是', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '0', '邮件推广', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '1', '电话推广', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '2', '网上搜索', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '3', '朋友介绍', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '4', '线下拜访', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_SOURCE', '5', '其他', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '0', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '1', '已分配', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '2', '已认领', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '3', '已搁置', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CLUE_STATE', '4', '已关闭', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'app_code_define', '应用编码', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CODE_TYPE_GROUP', 'sys_code_define', '系统编码', '系统编码123a1', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '0', '计算机/互联网/通信/电子', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '1', '会计/金融/银行/保险', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '10', '政府/非赢利机构/其他', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '11', '餐饮/娱乐', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '2', '贸易/消费/制造/营运', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '3', '制药/医疗', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '4', '广告/媒体', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '5', '房地产/建筑', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '6', '专业服务/教育/培训', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '7', '服务业', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '8', '物流/运输', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_INDUSTRY', '9', '能源/原材料', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_LEVEL', 'KEY', '重点', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_LEVEL', 'NON_PRIORITY', '非优先', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_LEVEL', 'ORDINARY', '普通', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '0', '未知', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '1', '外资(欧美)', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '10', '非营利机构', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '11', '其他性质', '', '12', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '2', '外资(非欧美)', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '3', '合资', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '4', '国企', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '5', '民营公司', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '6', '国内上市公司', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '7', '外企代表处', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '8', '政府机关', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_NATURE', '9', '事业单位', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'BUSINESS_NEGOTIATION', '商务洽谈', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'DETERMINED_INTENTION', '确定意向', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'PRELIMINARY', '初步进展', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'SIGNING_DEAL', '签约成交', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_PROGRESS_STATE', 'STAGNANT_LOSS', '停滞流失', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '1000-5000', '1000-5000人', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '10000', '10000人以上', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '150-500', '150-500人', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '50', '少于50人', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '50-150', '50-150人', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '500-1000', '500-1000人', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_SCALE', '5000-10000', '5000-10000人', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_STATE', 'Confirm', '确认', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_STATE', 'init', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_STATE', 'Postpone', '暂缓', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_STATE', 'Submit', '提交', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_VISIT_CATEGORY', 'FOLLOW_CUST', '我的客户拜访', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('CUST_VISIT_CATEGORY', 'PRO_CUST', '潜在客户拜访', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcmenu', '功能菜单', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('FUNCTION_TYPE', 'funcnode', '功能节点', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('GRP_STATE', '0', '无效', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('GRP_STATE', '1', '有效', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'MAIN', '主处理器', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('HANDLER_TYPE', 'OTHER', '其他处理器', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('INFO_TYPE', 'menu', '目录', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('INFO_TYPE', 'part', '角色', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '0', '关闭', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('MENUTREE_CASCADE', '1', '展开', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'disableMode', '不能操作', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPER_CTR_TYPE', 'hiddenMode', '隐藏按钮', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'crm', 'AEAI CRM', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'dp', 'AEAI DP', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'esb', 'AEAI ESB', '', '0', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'hr', 'AEAI HR', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'portal', 'AEAI PORTAL', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_CONCERN_PRODUCT', 'wm', 'AEAI WM', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_LEVEL', 'INTERMEDIATE', '中', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_LEVEL', 'STRONG', '强', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_LEVEL', 'WEAK', '弱', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_STATE', '0', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_STATE', '1', '已提交', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_STATE', '2', '已确认', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('OPP_STATE', '3', '已关闭', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('ORDER_STATE', '0', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORDER_STATE', '1', '提交', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORDER_STATE', '2', '已确认', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '1', '一级', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '2', '二级', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_RANK', '3', '三级', null, '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_TYPE', 'company', '公司', null, '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORGNAZITION_TYPE', 'department', '部门', null, '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'ENTERPRISE_ENTITY', '企业实体', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'SOFTWARE_AGENTS', '软件代理商', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'SOFTWARE_DEVELOPERS', '软件开发商', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'SYSTEM_INTEGRATOR', '系统集成商', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_CLASSIFICATION', 'UNKNOWN', '未知', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '1', '无人接听', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '2', '号码信息不符', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '3', '空号', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '4', '暂停服务', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '5', '暂无需求', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '6', '后续联系', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_LABELS', '7', '态度恶劣', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_SOURCES', 'INFO_ACQUIRE', '信息采集', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_SOURCES', 'RECRUIT_ACQUIRE', '招聘采集', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_SOURCES', 'WEB_SEARCH', '网络搜索', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '0', '未知', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '1', '外资（欧美）', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '10', '非营利机构', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '11', '其他性质', '', '12', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '2', '外资（非欧美）', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '3', '合资', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '4', '国企', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '5', '民营公司', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '6', '国内上市公司', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '7', '外企代表处', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '8', '政府机关', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('ORG_TYPE', '9', '事业单位', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('PER_LABELS', '1', '不需要', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PER_LABELS', '2', '有意向', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('PER_LABELS', '3', '错误信息', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('PER_SEX', 'F', '女', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('PER_SEX', 'M', '男', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('PER_SEX', 'UNKONW', '未知', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PER_STATE', '0', '有效', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PER_STATE', '1', '无效', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'dummy_postion', '虚拟岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('POSITION_TYPE', 'real_postion', '实际岗位', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'AnHui', '安徽', '', '13', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'AoMen', '澳门', '', '33', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'BeiJing', '北京', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ChongQing', '重庆', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'FuJian', '福建', '', '14', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'GanSu', '甘肃', '', '28', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'GuangDong', '广东', '', '20', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'GuangXi', '广西', '', '21', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'GuiZhou', '贵州', '', '24', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HaiNan', '海南', '', '22', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HeBei', '河北', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HeiLongJiang', '黑龙江', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HeNan', '河南', '', '17', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HuBei', '湖北', '', '18', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'HuNan', '湖南', '', '19', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'JiangSu', '江苏', '', '11', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'JiangXi', '江西', '', '15', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'JiLin', '吉林', '', '9', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'LiaoNing', '辽宁', '', '8', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'NeiMengGu', '内蒙古', '', '7', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'NingXia', '宁夏', '', '30', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'QingHai', '青海', '', '29', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'SanXi', '陕西', '', '27', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ShanDong', '山东', '', '16', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ShangHai', '上海', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ShanXi', '山西', '', '6', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'SiChuan', '四川', '', '23', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'TaiWan', '台湾', '', '34', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'TianJin', '天津', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'XiangGang', '香港', '', '32', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'XinJiang', '新疆', '', '31', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'XiZang', '西藏', '', '26', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'YunNan', '云南', '', '25', '1');
INSERT INTO `sys_codelist` VALUES ('PROVINCE', 'ZheJiang', '浙江', '', '12', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'IMAGE', '图片文件', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'ISO', '镜像文件', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('RES_TYPE', 'VIDEO', '视频文件', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '0', '无效', 'null', '2', '1');
INSERT INTO `sys_codelist` VALUES ('SYS_VALID_TYPE', '1', '有效', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_CLASS', 'ColdCalls', '陌生拜访', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_CLASS', 'FollowUp', '意向跟进', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_FILTE', 'HISTORY', '历史跟进', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_FILTE', 'LASTWEEK', '上周跟进', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_FILTE', 'NOTFOR', '未进行', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_FOLLOW_STATUS', 'HaveFollowUp', '已跟进', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_FOLLOW_STATUS', 'HaveFollowUpInit', '已跟进(*)', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_FOLLOW_STATUS', 'Init', '初始化', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_FOLLOW_STATUS', 'NoFollowUp', '未跟进', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_REVIEW_STATE', 'ConfirmPlan', '确认计划', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_REVIEW_STATE', 'ConfirmSummary', '确认总结', '', '5', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_REVIEW_STATE', 'Init', '初始化', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_REVIEW_STATE', 'SubmitPlan', '提交计划', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('TASK_REVIEW_STATE', 'SubmitSummary', '提交总结', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'dept', '部门', '', '10', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'org', '机构', '', '20', '1');
INSERT INTO `sys_codelist` VALUES ('UNIT_TYPE', 'post', '岗位', '', '30', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'F', '女', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('USER_SEX', 'M', '男', 'null', '1', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_EFFECT', 'Follow_up', '可跟进', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_EFFECT', 'Have_intention', '有意向', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_EFFECT', 'Unwanted', '不需要', '', '2', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_TYPE', '0', '预约拜访', '', '1', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_TYPE', '1', '电话拜访', '', '3', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_TYPE', '2', '邮件拜访', '', '4', '1');
INSERT INTO `sys_codelist` VALUES ('VISIT_TYPE', '3', '陌生拜访', '', '2', '1');

-- ----------------------------
-- Table structure for sys_codetype
-- ----------------------------
DROP TABLE IF EXISTS `sys_codetype`;
CREATE TABLE `sys_codetype` (
  `TYPE_ID` varchar(32) NOT NULL,
  `TYPE_NAME` varchar(32) DEFAULT NULL,
  `TYPE_GROUP` varchar(32) DEFAULT NULL,
  `TYPE_DESC` varchar(128) DEFAULT NULL,
  `IS_CACHED` char(1) DEFAULT NULL,
  `IS_UNITEADMIN` char(1) DEFAULT NULL,
  `IS_EDITABLE` char(1) DEFAULT NULL,
  `LEGNTT_LIMIT` varchar(6) DEFAULT NULL,
  `CHARACTER_LIMIT` char(1) DEFAULT NULL,
  `EXTEND_SQL` char(1) DEFAULT NULL,
  `SQL_BODY` varchar(512) DEFAULT NULL,
  `SQL_COND` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_codetype
-- ----------------------------
INSERT INTO `sys_codetype` VALUES ('AuthedHandlerId', '认证Handler定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '', 'B', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('BOOL_DEFINE', '布尔定义', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'C', '', '', '');
INSERT INTO `sys_codetype` VALUES ('CLUE_SOURCE', '来源方式', 'sys_code_define', null, 'Y', 'Y', 'Y', '', null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('CLUE_STATE', '线索状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '12', '', null, null, null);
INSERT INTO `sys_codetype` VALUES ('CODE_TYPE_GROUP', '编码类型分组', 'app_code_define', '编码类型分组', null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('CUST_INDUSTRY', '行业', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_LEVEL', '客户等级', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_NATURE', '性质', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_PROGRESS_STATE', '客户进展状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_SCALE', '规模', 'sys_code_define', '', 'Y', 'Y', 'Y', '24', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_STATE', '状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('CUST_VISIT_CATEGORY', '客户拜访类别', 'app_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('FUNCTION_TYPE', '功能类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('GRP_STATE', '客户分组状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('HANDLER_TYPE', '控制器类型', 'sys_code_define', '', 'N', 'Y', 'Y', '32', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('MENUTREE_CASCADE', '是否展开', 'sys_code_define', '', 'Y', 'Y', 'Y', '1', 'N', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPER_CTR_TYPE', '操作控制类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('OPP_CONCERN_PRODUCT', '产品类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPP_LEVEL', '商机级别', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('OPP_STATE', '商机状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORDER_STATE', '订单状态', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORGNAZITION_TYPE', '组织类别', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORG_CLASSIFICATION', '企业分类', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORG_LABELS', '潜在客户标签', 'sys_code_define', '', 'Y', 'Y', 'Y', '32', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORG_SOURCES', '潜在客户来源渠道', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('ORG_TYPE', '组织资源', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('PER_LABELS', '潜在个体客户标签', 'sys_code_define', '', 'Y', 'Y', 'Y', '32', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('PER_SEX', '个人性别类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('PER_STATE', '潜在用户状态', 'sys_code_define', null, 'Y', 'Y', 'Y', '', null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('POSITION_TYPE', '岗位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('PROVINCE', '省份', 'sys_code_define', '', 'Y', 'Y', 'Y', '20', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('RES_TYPE', '资源类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('SYS_VALID_TYPE', '有效标识符', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('TASK_CLASS', '我的任务类别', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('TASK_FILTE', '任务过滤', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('TASK_FOLLOW_STATUS', '任务跟进状态', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('TASK_REVIEW_STATE', '我的任务状态', 'app_code_define', '', 'Y', 'Y', 'Y', '', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('UNIT_TYPE', '单位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO `sys_codetype` VALUES ('USER_SEX', '性别类型', 'sys_code_define', '', 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO `sys_codetype` VALUES ('VISIT_EFFECT', '沟通效果', 'sys_code_define', '', 'Y', 'Y', 'Y', '32', '', 'N', '', '');
INSERT INTO `sys_codetype` VALUES ('VISIT_TYPE', '拜访类型', 'sys_code_define', '', 'Y', 'Y', 'Y', '10', '', 'N', '', '');

-- ----------------------------
-- Table structure for sys_function
-- ----------------------------
DROP TABLE IF EXISTS `sys_function`;
CREATE TABLE `sys_function` (
  `FUNC_ID` varchar(36) NOT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `FUNC_TYPE` varchar(32) DEFAULT NULL,
  `MAIN_HANDLER` varchar(36) DEFAULT NULL,
  `FUNC_PID` varchar(36) DEFAULT NULL,
  `FUNC_STATE` char(1) DEFAULT NULL,
  `FUNC_SORT` int(11) DEFAULT NULL,
  `FUNC_DESC` varchar(256) DEFAULT NULL,
  `FUNC_ICON` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`FUNC_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_function
-- ----------------------------
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000000', '客户管理系统', 'funcmenu', null, null, '1', null, null, null);
INSERT INTO `sys_function` VALUES ('00000000-0000-0000-00000000000000001', '系统管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '101', '', null);
INSERT INTO `sys_function` VALUES ('029FB2F2-370F-46D5-A283-A22C8E49341C', '客户管理', 'funcmenu', '', '00000000-0000-0000-00000000000000000', '1', '99', '', null);
INSERT INTO `sys_function` VALUES ('0BC027DB-9D9C-4A69-9318-384966FA6CFE', '任务周期', 'funcnode', '7CE41386-CD00-46E6-B9D4-8556C8B32A02', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '8', '', null);
INSERT INTO `sys_function` VALUES ('33CF8E06-1181-453B-ADF4-1F18ABC026CF', '客户分组', 'funcnode', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '5', '', null);
INSERT INTO `sys_function` VALUES ('39C984AD-921C-4C74-BB1F-DD98597F7C01', '人员管理', 'funcnode', 'B4531E62-1F5B-4341-BB7A-6BFD1CA1BDEA', '00000000-0000-0000-00000000000000001', '1', '3', '', null);
INSERT INTO `sys_function` VALUES ('3F23DD59-DB0D-4A25-851F-39E63E3944ED', '潜在客户', 'funcnode', '2287114B-17C2-4E88-8A2F-505709ADB719', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '2', '', null);
INSERT INTO `sys_function` VALUES ('5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6', '附件管理', 'funcnode', '1F617665-FC8B-4E8C-ABE4-540C363A17A8', '00000000-0000-0000-00000000000000001', '1', '8', '', null);
INSERT INTO `sys_function` VALUES ('67BA273A-DD31-48D0-B78C-1D60D5316074', '系统日志', 'funcnode', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', '00000000-0000-0000-00000000000000001', '1', '7', null, null);
INSERT INTO `sys_function` VALUES ('692B0D37-2E66-4E82-92B4-E59BCF76EE76', '编码管理', 'funcnode', 'B4FE5722-9EA6-47D8-8770-D999A3F6A354', '00000000-0000-0000-00000000000000001', '1', '6', null, null);
INSERT INTO `sys_function` VALUES ('868188AB-99B3-4274-9F2F-66A0D25F2725', '我的任务', 'funcnode', 'BFC4B5D7-53BD-4F17-A493-C7FBD49C40CE', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '9', '', null);
INSERT INTO `sys_function` VALUES ('8C84B439-2788-4608-89C4-8F5AA076D124', '组织机构', 'funcnode', '439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', '00000000-0000-0000-00000000000000001', '1', '1', null, null);
INSERT INTO `sys_function` VALUES ('A0334956-426E-4E49-831B-EB00E37285FD', '编码类型', 'funcnode', '9A16D554-F989-438A-B92D-C8C8AC6BF9B8', '00000000-0000-0000-00000000000000001', '1', '5', null, null);
INSERT INTO `sys_function` VALUES ('C977BC31-C78F-4B16-B0C6-769783E46A06', '功能管理', 'funcnode', '46C52D33-8797-4251-951F-F7CA23C76BD7', '00000000-0000-0000-00000000000000001', '1', '4', null, null);
INSERT INTO `sys_function` VALUES ('CAF74986-8A56-45FD-9FE7-406651938045', '我的客户', 'funcnode', '3743767A-8D65-4466-B0A3-D2C11F07B5EC', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '7', '', null);
INSERT INTO `sys_function` VALUES ('D16F9D22-079C-45A7-9C71-7B15A5BDFE21', '客户信息', 'funcnode', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '6', '', null);
INSERT INTO `sys_function` VALUES ('D3582A2A-3173-4F92-B1AD-2F999A2CBE18', '修改密码', 'funcnode', '88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', '00000000-0000-0000-00000000000000001', '1', '9', '', null);
INSERT INTO `sys_function` VALUES ('DFE8BE4C-4024-4A7B-8DF2-630003832AE9', '角色管理', 'funcnode', '0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', '00000000-0000-0000-00000000000000001', '1', '2', null, null);
INSERT INTO `sys_function` VALUES ('E71EA711-C3DF-4E47-8282-0F76D094D650', '任务审查', 'funcnode', 'BC08F9AB-E067-4F43-B5FE-B2F8C8D8BB39', '029FB2F2-370F-46D5-A283-A22C8E49341C', '1', '10', '', null);

-- ----------------------------
-- Table structure for sys_handler
-- ----------------------------
DROP TABLE IF EXISTS `sys_handler`;
CREATE TABLE `sys_handler` (
  `HANLER_ID` varchar(36) NOT NULL,
  `HANLER_CODE` varchar(64) DEFAULT NULL,
  `HANLER_TYPE` varchar(32) DEFAULT NULL,
  `HANLER_URL` varchar(128) DEFAULT NULL,
  `FUNC_ID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`HANLER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_handler
-- ----------------------------
INSERT INTO `sys_handler` VALUES ('001C1892-C7E7-4630-AC05-F817134767CD', 'MyCustVisitCreateContEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('02A99C63-62C6-4742-8822-E7A470831DA2', 'ContListSelectList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('064E773E-D9D3-4103-9821-0B723CFEC605', 'CustomerListSelectList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('07476B81-8E76-4653-8F2B-30CD1ED810DD', 'MyTasksOrderInfoManageEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', 'SecurityRoleTreeManage', 'MAIN', '', 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO `sys_handler` VALUES ('0E542F5D-E4A9-4F0F-B413-A5C62E462C52', 'MyCustomerInfoEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('0E7B86FF-DEA5-46AE-A7BE-826798F2EA87', 'MyCustVisitInfo', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('0E93034E-C17B-4233-BDD8-C67B132F2911', 'SecurityUserQuery', 'OTHER', '', '39C984AD-921C-4C74-BB1F-DD98597F7C01');
INSERT INTO `sys_handler` VALUES ('116C2F08-70F7-4CD8-9485-BC18C57043F1', 'MyCustSalesmanListSelectList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('1537B0B1-C9FC-4BDC-B4C0-DFF80EEE394E', 'SecurityUserRGManageList', 'OTHER', '', '39C984AD-921C-4C74-BB1F-DD98597F7C01');
INSERT INTO `sys_handler` VALUES ('15610E0B-07DB-40A6-95BF-F457F969DA51', 'MyCustOppCreateContEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('170B3848-9BE0-490B-A9D9-48018DA41CA7', 'MyCustomerSelectList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('172BEAD9-1336-43DC-B09E-94EDF34F6A91', 'SalerListSelectList', 'OTHER', '', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `sys_handler` VALUES ('1F617665-FC8B-4E8C-ABE4-540C363A17A8', 'WcmGeneralGroup8ContentList', 'MAIN', null, '5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6');
INSERT INTO `sys_handler` VALUES ('1F63F91C-1EAB-4116-B36D-9918E602C84C', 'ReviewFollowUpList', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('2287114B-17C2-4E88-8A2F-505709ADB719', 'OrgInfoManageList', 'MAIN', '', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `sys_handler` VALUES ('2A26CAD1-69BD-4D89-9F29-1D6B55CA83F3', 'MyTasksSalesmanListSelectList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('2A821AC2-84B2-4371-9DEC-BABEF77D3FE1', 'PostponeManage', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('2B0866CA-1264-4989-B178-3868EDC725A5', 'CustomerSalesInfo', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('2BF0DA71-78A0-4C4E-9722-A7715C64B665', 'ProCustomerSelectList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('32820B22-4830-4BFD-B3C6-911271C3A882', 'MyTasksContListSelectList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('3743767A-8D65-4466-B0A3-D2C11F07B5EC', 'MyCustomerInfoManageList', 'MAIN', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', 'SecurityGroupList', 'MAIN', '', '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO `sys_handler` VALUES ('45417D72-CD18-46C9-842F-BDB7835F7503', 'MyCustOrderInfoManageList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('46C52D33-8797-4251-951F-F7CA23C76BD7', 'FunctionTreeManage', 'MAIN', null, 'C977BC31-C78F-4B16-B0C6-769783E46A06');
INSERT INTO `sys_handler` VALUES ('483F3A88-1D8F-4229-9BBB-A242FBC966A7', 'VisitCreateClueEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'SysLogQueryList', 'MAIN', null, '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO `sys_handler` VALUES ('4BA0EBBB-656B-4D64-8DC5-27914628EA01', 'MyTasksOppCreateContEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('4DA6AFBA-56A5-45FB-A3F6-3EFACAEFEFD6', 'MyTasksOppInfoManageList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('4E27B443-2F2B-432E-99D6-3BDA1313FD7A', 'FollowUpManage', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('5105081E-C32F-4959-AAEC-75080B4A44EC', 'MyTaskCustInfoEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('53AB4B41-87EA-46E6-AA8D-FC1B29B0402E', 'ProcustVisitManageEdit', 'OTHER', '', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `sys_handler` VALUES ('53C21063-FC88-484B-9AB9-53E694E5913B', 'SalesmanListSelectList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('54E95238-A212-4010-AD75-80098FE2C210', 'FollowUpManageEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('593CFD80-AE6D-490C-A49D-5C897A515351', 'VisitListSelectList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('5B1FD542-4246-4DEE-86A9-F38EFA67084C', 'SecurityUserManageTreePick', 'OTHER', '', '39C984AD-921C-4C74-BB1F-DD98597F7C01');
INSERT INTO `sys_handler` VALUES ('5B6D668B-A7A3-4884-9A52-87E9FDE4E226', 'ProcustVisitManageList', 'OTHER', '', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `sys_handler` VALUES ('5E7EEA22-7706-4C7D-AA82-E48A6F16D984', 'ReviewVisitManageEdit', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('613C474D-442A-4ED2-A426-F11E66CCD961', 'CustomerSalesPersonnelInfoSelect', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('61500B0F-92F0-4283-B6AC-42759BE2599A', 'MyTasksVisitManageEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('61BD908F-3585-47F8-B9EC-AD7E3C6944D9', 'DataStatistics', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('61F77B54-32D4-477B-B2AB-7B6ECF731FED', 'ColdCallsManageList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('624DC816-58E9-4B7E-949E-D806CE2E4E0D', 'CustomerGroupPick', 'OTHER', '', '33CF8E06-1181-453B-ADF4-1F18ABC026CF');
INSERT INTO `sys_handler` VALUES ('6727177D-138A-4E0C-815A-33BA55455697', 'MyCustOrderEntryEditBox', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('6906AB63-2BD6-11E6-9848-D8B80F238224', 'OppCreateContEdit', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('690E17D8-2BD6-11E6-9848-D8B80F238224', 'OppCreateOrderInfoEdit', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('69167A4B-2BD6-11E6-9848-D8B80F238224', 'OppInfoManageEdit', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('691C727A-2BD6-11E6-9848-D8B80F238224', 'OppInfoManageList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('6923DFE2-2BD6-11E6-9848-D8B80F238224', 'OrderEntryEditBox', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('692AC8CC-2BD6-11E6-9848-D8B80F238224', 'OrderInfoManageEdit', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('6933612D-2BD6-11E6-9848-D8B80F238224', 'OrderInfoManageList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('6934FABF-8887-43D9-BD23-FC7F0D8D39BB', 'WorkSummary', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('693BBBBF-2BD6-11E6-9848-D8B80F238224', 'VisitContListSelectList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('6951D10C-2BD6-11E6-9848-D8B80F238224', 'VisitCreateContEdit', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('695A2FFF-2BD6-11E6-9848-D8B80F238224', 'VisitListSelectList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('6960470D-2BD6-11E6-9848-D8B80F238224', 'VisitManageEdit', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('6967F1BA-2BD6-11E6-9848-D8B80F238224', 'VisitManageList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('6EA4842D-3F1A-4624-B926-7D954DB9C5BE', 'MyCustOrderInfoManageEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('78D6D181-10E7-4E24-9B4A-DEB4080FFCB4', 'MyTasksVisitCreateContEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('7A02F01D-FB85-4F73-AB65-F14E9E831EBF', 'CustomerGroup8ContentList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('7A2A894F-4417-432E-9A37-7E31ACE782CC', 'MyCustomerSalesPersonnelInfoSelect', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('7A54AEAA-C201-48B6-BF27-9FBDF321E8E1', 'ReviewVisitInfo', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('7C9AA3E1-3CFB-4BF0-B60B-A89A34F62208', 'MyCustProductTreeSelect', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('7CE41386-CD00-46E6-B9D4-8556C8B32A02', 'TaskCycleManageList', 'MAIN', null, '0BC027DB-9D9C-4A69-9318-384966FA6CFE');
INSERT INTO `sys_handler` VALUES ('873BB0CC-742E-4531-B44C-45C11B72F381', 'MyTasksVisitCreateClueEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', 'ModifyPassword', 'MAIN', null, 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO `sys_handler` VALUES ('894F3E00-FF69-494E-A072-5D63D185097E', 'CustomerInfoEdit', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('8AB47828-0391-41BD-8990-B6EA3D950A03', 'MyCustVisitManageEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('9693FE53-01C5-41F3-B7AE-8C4848155A1E', 'TaskCycleSelect', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('96DCFC52-F873-4360-8A52-AC9D30693F78', 'CustomerSelectList', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('991B35BA-8FA0-4814-9D1E-0AB373B6BB68', 'ProcustUpload', 'OTHER', '', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `sys_handler` VALUES ('9A16D554-F989-438A-B92D-C8C8AC6BF9B8', 'CodeTypeManageList', 'MAIN', null, 'A0334956-426E-4E49-831B-EB00E37285FD');
INSERT INTO `sys_handler` VALUES ('9AFF8ACB-2114-4BD2-86A4-32151D3B7E85', 'ReviewOppInfoManageEdit', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('9C7EA9C9-D385-451B-80D5-6029C525B957', 'MyCustOppInfoManageEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('9CB4802D-9531-4982-88D1-C05AA99798C1', 'ProductTreeSelect', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'CustomerGroupManageList', 'MAIN', '', '33CF8E06-1181-453B-ADF4-1F18ABC026CF');
INSERT INTO `sys_handler` VALUES ('A4AF80B7-7274-4827-9406-3526B475C271', 'MyTasksOppInfoManageEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('AD089F06-9BB1-4E68-BFB1-74E8C9941926', 'MyTasksVisitManageList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'CustomerSale8ContentList', 'MAIN', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('B266C849-8771-49E9-8BC8-97440CBB824B', 'ProcustInfoEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('B2DECB20-5305-43C3-81C2-9FD69C3BC1B6', 'ReviewColdCallsEdit', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('B3F27298-B879-4917-BD7D-0C08111B4660', 'MyTasksProductTreeSelect', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('B4531E62-1F5B-4341-BB7A-6BFD1CA1BDEA', 'SecurityUserList', 'MAIN', null, '39C984AD-921C-4C74-BB1F-DD98597F7C01');
INSERT INTO `sys_handler` VALUES ('B4B3A774-0F09-492E-A5F9-D1457C8882DD', 'MyCustOppInfoManageList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('B4FE5722-9EA6-47D8-8770-D999A3F6A354', 'CodeListManageList', 'MAIN', null, '692B0D37-2E66-4E82-92B4-E59BCF76EE76');
INSERT INTO `sys_handler` VALUES ('B630F7A0-CDE9-4085-8F17-084D5D85997E', 'MyTasksOrderInfoManageList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('BC08F9AB-E067-4F43-B5FE-B2F8C8D8BB39', 'TaskReview', 'MAIN', null, 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('BDEB0106-6320-4916-B90A-143E43A260C8', 'MyTasksOrderEntryEditBox', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('BF67FC16-BB57-4A9E-8505-7B23F4F1B436', 'ReviewOrderInfoManageList', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('BFC4B5D7-53BD-4F17-A493-C7FBD49C40CE', 'TaskCycle8ContentManageList', 'MAIN', null, '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('C6B3B680-ABA5-49F4-8B0A-624075089AB6', 'ReviewFollowUpEdit', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('C80B9739-43BC-4B21-A7B4-FFB5ADD0F617', 'ColdCallsManageEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('CF68FC5E-4516-4838-92D2-D1DCD1233E84', 'ReviewColdCallsList', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('D0216E24-9FA2-4E94-A5C3-6D54017E6EEB', 'MyTasksOppCreateOrderInfoEdit', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('D4311B69-68AD-4363-B569-14434F2BB35D', 'ReviewVisitManageList', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('D5D502DE-3DC6-4945-AE80-EC03D01D9AD9', 'ReviewOppInfoManageList', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('D7FE5C6F-3601-444D-8219-9D17DCE3480A', 'MyCustVisitManageList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('DA98059A-2A55-4F92-8AB4-0EB09E5E403A', 'ReviewOrderInfoManageEdit', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('DEE3574E-1160-45C8-B504-B2B932321D7C', 'MyCustVisitCreateClueEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('DFCF22BF-7DFF-4D1E-AA17-507440ECB812', 'ReviewSummary8ContentList', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('DFE29979-7605-407D-A3F4-D6F5549974E9', 'MyCustContListSelectList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('E5B2521C-913B-423A-9BCC-31391584E87C', 'MyCustOppCreateOrderInfoEdit', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('E8DB4194-099F-4A53-8F74-0A61C8F96170', 'TaskCycleManageEdit', 'OTHER', '', '0BC027DB-9D9C-4A69-9318-384966FA6CFE');
INSERT INTO `sys_handler` VALUES ('EFABF371-69A1-4985-A285-4F2BE1EE389F', 'FollowUpManageList', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('EFFB120F-1C79-48EE-A9C2-5C28A88F0EDD', 'VisitContListSelectList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('F383F84D-35A2-4FD0-9476-96D489BCB4D9', 'ProCustVisitInfo', 'OTHER', '', '868188AB-99B3-4274-9F2F-66A0D25F2725');
INSERT INTO `sys_handler` VALUES ('F63CF10B-C61B-448B-BA04-1D07BD6927ED', 'OrgInfoManageEdit', 'OTHER', '', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');
INSERT INTO `sys_handler` VALUES ('F6CE5057-8BCA-4B38-A737-CD2A3D7F0A6D', 'CustomerSalePick', 'OTHER', '', 'D16F9D22-079C-45A7-9C71-7B15A5BDFE21');
INSERT INTO `sys_handler` VALUES ('FD8FC69D-429F-425F-8C75-558046E727E4', 'ReviewSummary', 'OTHER', '', 'E71EA711-C3DF-4E47-8282-0F76D094D650');
INSERT INTO `sys_handler` VALUES ('FDCF94BF-EE02-465F-9FEB-7D76D5DD4288', 'ProvinceListSelectList', 'OTHER', '', 'CAF74986-8A56-45FD-9FE7-406651938045');
INSERT INTO `sys_handler` VALUES ('FE46C2D2-9BD5-4DEA-8A04-AAC5CA4AB1C9', 'LabelsTreeSelect', 'OTHER', '', '3F23DD59-DB0D-4A25-851F-39E63E3944ED');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `ID` char(36) DEFAULT NULL,
  `OPER_TIME` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP_ADDTRESS` varchar(32) DEFAULT NULL,
  `USER_ID` varchar(32) DEFAULT NULL,
  `USER_NAME` varchar(32) DEFAULT NULL,
  `FUNC_NAME` varchar(64) DEFAULT NULL,
  `ACTION_TYPE` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_onlinecount
-- ----------------------------
DROP TABLE IF EXISTS `sys_onlinecount`;
CREATE TABLE `sys_onlinecount` (
  `IPADDRRESS` varchar(64) NOT NULL,
  `ONLINECOUNT` int(11) DEFAULT NULL,
  PRIMARY KEY (`IPADDRRESS`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_onlinecount
-- ----------------------------

-- ----------------------------
-- Table structure for sys_operation
-- ----------------------------
DROP TABLE IF EXISTS `sys_operation`;
CREATE TABLE `sys_operation` (
  `OPER_ID` char(36) NOT NULL,
  `HANLER_ID` varchar(36) DEFAULT NULL,
  `OPER_CODE` varchar(64) DEFAULT NULL,
  `OPER_NAME` varchar(64) DEFAULT NULL,
  `OPER_ACTIONTPYE` varchar(64) DEFAULT NULL,
  `OPER_SORT` int(11) DEFAULT NULL,
  PRIMARY KEY (`OPER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_operation
-- ----------------------------
INSERT INTO `sys_operation` VALUES ('01E6A560-0552-46BC-8A5F-0C2BEB987617', '894F3E00-FF69-494E-A072-5D63D185097E', 'cancelBtn', '取消从', 'cancelBtn', '11');
INSERT INTO `sys_operation` VALUES ('02E89343-F996-44EC-8347-6FB3C60BC9BB', '894F3E00-FF69-494E-A072-5D63D185097E', 'submit', '提交', 'submit', '4');
INSERT INTO `sys_operation` VALUES ('046C0FC7-2BD7-11E6-9848-D8B80F238224', '6967F1BA-2BD6-11E6-9848-D8B80F238224', 'create', '新增', 'create', '3');
INSERT INTO `sys_operation` VALUES ('04729D62-2BD7-11E6-9848-D8B80F238224', '6967F1BA-2BD6-11E6-9848-D8B80F238224', 'edit', '编辑', 'edit', '4');
INSERT INTO `sys_operation` VALUES ('0481B2B5-2BD7-11E6-9848-D8B80F238224', '6967F1BA-2BD6-11E6-9848-D8B80F238224', 'visitAgain', '再次拜访', 'visitAgain', '5');
INSERT INTO `sys_operation` VALUES ('048A692B-2BD7-11E6-9848-D8B80F238224', '6967F1BA-2BD6-11E6-9848-D8B80F238224', 'reConfirm', '反确认', 'reConfirm', '8');
INSERT INTO `sys_operation` VALUES ('0494E074-2BD7-11E6-9848-D8B80F238224', '6967F1BA-2BD6-11E6-9848-D8B80F238224', 'confirm', '确认', 'confirm', '7');
INSERT INTO `sys_operation` VALUES ('049CFB7A-2BD7-11E6-9848-D8B80F238224', '6967F1BA-2BD6-11E6-9848-D8B80F238224', 'generateClue', '生成商机', 'generateClue', '6');
INSERT INTO `sys_operation` VALUES ('04AAF43E-2BD7-11E6-9848-D8B80F238224', '6967F1BA-2BD6-11E6-9848-D8B80F238224', 'detail', '查看', 'detail', '1');
INSERT INTO `sys_operation` VALUES ('04C5A622-2BD7-11E6-9848-D8B80F238224', '6967F1BA-2BD6-11E6-9848-D8B80F238224', 'delete', '删除', 'delete', '2');
INSERT INTO `sys_operation` VALUES ('06636501-26DD-11E6-97B9-EE4FBFD2DDDB', '9C7EA9C9-D385-451B-80D5-6029C525B957', 'back', '返回', 'back', '1');
INSERT INTO `sys_operation` VALUES ('0669FE86-26DD-11E6-97B9-EE4FBFD2DDDB', '9C7EA9C9-D385-451B-80D5-6029C525B957', 'close', '关闭', 'close', '8');
INSERT INTO `sys_operation` VALUES ('06714CF1-26DD-11E6-97B9-EE4FBFD2DDDB', '9C7EA9C9-D385-451B-80D5-6029C525B957', 'reConfirm', '反确认', 'reConfirm', '7');
INSERT INTO `sys_operation` VALUES ('06787193-26DD-11E6-97B9-EE4FBFD2DDDB', '9C7EA9C9-D385-451B-80D5-6029C525B957', 'save', '保存', 'save', '3');
INSERT INTO `sys_operation` VALUES ('0680D515-26DD-11E6-97B9-EE4FBFD2DDDB', '9C7EA9C9-D385-451B-80D5-6029C525B957', 'edit', '编辑', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('069182D7-26DD-11E6-97B9-EE4FBFD2DDDB', '9C7EA9C9-D385-451B-80D5-6029C525B957', 'confirm', '确认', 'confirm', '6');
INSERT INTO `sys_operation` VALUES ('069ABAD4-26DD-11E6-97B9-EE4FBFD2DDDB', '9C7EA9C9-D385-451B-80D5-6029C525B957', 'reSubmit', '反提交', 'reSubmit', '5');
INSERT INTO `sys_operation` VALUES ('06A4492F-26DD-11E6-97B9-EE4FBFD2DDDB', '9C7EA9C9-D385-451B-80D5-6029C525B957', 'submit', '提交', 'submit', '4');
INSERT INTO `sys_operation` VALUES ('0831D3B9-7BD5-4581-A540-72982B899171', '894F3E00-FF69-494E-A072-5D63D185097E', 'save', '保存', 'save', '3');
INSERT INTO `sys_operation` VALUES ('0A92F33C-5E91-4401-99C2-1FB41C8BE0B9', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'cancelBtn', '取消从', 'cancelBtn', '1');
INSERT INTO `sys_operation` VALUES ('0D04FA93-2BD9-11E6-9848-D8B80F238224', '692AC8CC-2BD6-11E6-9848-D8B80F238224', 'confirm', '确认', 'confirm', '3');
INSERT INTO `sys_operation` VALUES ('0D0B88AA-2BD9-11E6-9848-D8B80F238224', '692AC8CC-2BD6-11E6-9848-D8B80F238224', 'reConfirm', '反确认', 'reConfirm', '4');
INSERT INTO `sys_operation` VALUES ('0D1352DA-2BD9-11E6-9848-D8B80F238224', '692AC8CC-2BD6-11E6-9848-D8B80F238224', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('0D1BD201-2BD9-11E6-9848-D8B80F238224', '692AC8CC-2BD6-11E6-9848-D8B80F238224', 'save', '保存', 'save', '2');
INSERT INTO `sys_operation` VALUES ('0D228223-2BD9-11E6-9848-D8B80F238224', '692AC8CC-2BD6-11E6-9848-D8B80F238224', 'editBtn', '编辑从', 'editBtn', '6');
INSERT INTO `sys_operation` VALUES ('0D29E300-2BD9-11E6-9848-D8B80F238224', '692AC8CC-2BD6-11E6-9848-D8B80F238224', 'back', '返回', 'back', '5');
INSERT INTO `sys_operation` VALUES ('0D2FF6A7-2BD9-11E6-9848-D8B80F238224', '692AC8CC-2BD6-11E6-9848-D8B80F238224', 'deleteBtn', '删除从', 'deleteBtn', '7');
INSERT INTO `sys_operation` VALUES ('0D377516-2BD9-11E6-9848-D8B80F238224', '692AC8CC-2BD6-11E6-9848-D8B80F238224', 'createBtn', '新增从', 'createBtn', '8');
INSERT INTO `sys_operation` VALUES ('17FB686F-8580-4995-8494-EC42F9C71FC0', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'delete', '删除', 'delete', '3');
INSERT INTO `sys_operation` VALUES ('199DCC87-DB0D-4F78-A0AE-CCE348B13516', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'delivery', '分发', 'delivery', '10');
INSERT INTO `sys_operation` VALUES ('1B18D08B-2066-4259-A9AE-4987FB58C3E4', '3743767A-8D65-4466-B0A3-D2C11F07B5EC', 'delete', '删除', 'delete', '4');
INSERT INTO `sys_operation` VALUES ('1BF7D254-304B-4719-BCB6-AB38BF8CA4E7', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'edit', '编辑', 'edit', '5');
INSERT INTO `sys_operation` VALUES ('2321E4A1-2CB5-430C-8841-5129D02D2C38', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'delete', '删除', 'delete', '1');
INSERT INTO `sys_operation` VALUES ('24A26858-4628-472E-810B-964E817E7176', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'detail', '查看', 'detail', '9');
INSERT INTO `sys_operation` VALUES ('262F2106-D50E-4BDF-9FFD-562012596CC7', '2B0866CA-1264-4989-B178-3868EDC725A5', 'refresh', '刷新', 'refresh', '3');
INSERT INTO `sys_operation` VALUES ('28DFDD64-D05F-4D87-A997-11F29FA27042', '9F4DFA03-9114-4268-87C6-26B8EE3606D3', 'up', '上移', 'up', '5');
INSERT INTO `sys_operation` VALUES ('2A03F130-73A4-4A11-A03A-121BE4B1BFA0', '2287114B-17C2-4E88-8A2F-505709ADB719', 'excel', '导入', 'excel', '6');
INSERT INTO `sys_operation` VALUES ('2CAE2167-AA7B-437D-8953-C0E392B15AFC', '2287114B-17C2-4E88-8A2F-505709ADB719', 'edit', '编辑', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('34D36130-6300-425B-8820-E05C1EF2A2E6', 'AE104817-4754-49AA-9C29-BCB9EAFC9C22', 'filter', '过滤', 'filter', '2');
INSERT INTO `sys_operation` VALUES ('35067AE0-9243-450A-B9D7-59B550CDD8D2', 'F63CF10B-C61B-448B-BA04-1D07BD6927ED', 'back', '返回', 'back', '4');
INSERT INTO `sys_operation` VALUES ('61DF22C0-98A7-4D4B-8B7B-B6A29FCB375F', 'F63CF10B-C61B-448B-BA04-1D07BD6927ED', 'edit', '编辑', 'edit', '1');
INSERT INTO `sys_operation` VALUES ('6C16A2E5-0128-4D93-84C3-E9BB9F2327B6', '3743767A-8D65-4466-B0A3-D2C11F07B5EC', 'edit', '编辑', 'edit', '2');
INSERT INTO `sys_operation` VALUES ('6D3B756C-739B-4075-A69C-750F539223B3', '2287114B-17C2-4E88-8A2F-505709ADB719', 'create', '新增', 'create', '1');
INSERT INTO `sys_operation` VALUES ('7211605C-F7D5-4C45-8918-B3FC938F5EF5', '2287114B-17C2-4E88-8A2F-505709ADB719', 'detail', '查看', 'detail', '3');
INSERT INTO `sys_operation` VALUES ('790FA246-8461-4607-A046-903A2C8DE118', '2287114B-17C2-4E88-8A2F-505709ADB719', 'assign', '分配', 'assign', '5');
INSERT INTO `sys_operation` VALUES ('7D68E2A0-C40D-46BE-99B5-4BE51947386D', 'F63CF10B-C61B-448B-BA04-1D07BD6927ED', 'save', '保存', 'save', '2');
INSERT INTO `sys_operation` VALUES ('8EF8E532-1B93-440D-B0D6-680D7F72207B', '3743767A-8D65-4466-B0A3-D2C11F07B5EC', 'detail', '查看', 'detail', '3');
INSERT INTO `sys_operation` VALUES ('B5CD6899-DECC-450D-A69E-BF82B0A5DAD9', '3743767A-8D65-4466-B0A3-D2C11F07B5EC', 'create', '新增', 'create', '1');
INSERT INTO `sys_operation` VALUES ('BACCCF29-7D2E-4C49-A03A-F1F63DDBBA15', '2287114B-17C2-4E88-8A2F-505709ADB719', 'delete', '删除', 'delete', '4');
INSERT INTO `sys_operation` VALUES ('FE700723-9D3F-46D8-9C02-949AF8F9B939', 'F63CF10B-C61B-448B-BA04-1D07BD6927ED', 'assign', '分配', 'assign', '3');

-- ----------------------------
-- Table structure for wcm_general_group
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_group`;
CREATE TABLE `wcm_general_group` (
  `GRP_ID` varchar(36) NOT NULL,
  `GRP_NAME` varchar(64) DEFAULT NULL,
  `GRP_PID` varchar(36) DEFAULT NULL,
  `GRP_ORDERNO` int(11) DEFAULT NULL,
  `GRP_IS_SYSTEM` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_DESC` varchar(32) DEFAULT NULL,
  `GRP_RES_TYPE_EXTS` varchar(128) DEFAULT NULL,
  `GRP_RES_SIZE_LIMIT` varchar(32) DEFAULT NULL,
  `GRP_DESC` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`GRP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_group
-- ----------------------------
INSERT INTO `wcm_general_group` VALUES ('64D3B2E9-361E-4877-8AFE-D5DEA36BEA68', '潜在客户', '77777777-7777-7777-7777-777777777777', '3', 'N', '所有', '*.xls;*.xlsx', '100M', '');
INSERT INTO `wcm_general_group` VALUES ('77777777-7777-7777-7777-777777777777', '附件目录', '', null, '', '', '', '', '');
INSERT INTO `wcm_general_group` VALUES ('A6018D88-8345-46EE-A452-CE362FAC72E2', '视频文件', '77777777-7777-7777-7777-777777777777', '1', 'Y', '视频', '*.mp4;*.3gp;*.wmv;*.avi;*.rm;*.rmvb;*.flv', '100MB', '');
INSERT INTO `wcm_general_group` VALUES ('CF35D1E6-102E-428A-B39C-0072D491D5B1', '业务附件', '77777777-7777-7777-7777-777777777777', '2', 'Y', '所有资源', '*.*', '2MB', '');

-- ----------------------------
-- Table structure for wcm_general_resource
-- ----------------------------
DROP TABLE IF EXISTS `wcm_general_resource`;
CREATE TABLE `wcm_general_resource` (
  `RES_ID` varchar(36) NOT NULL,
  `GRP_ID` varchar(36) DEFAULT NULL,
  `RES_NAME` varchar(64) DEFAULT NULL,
  `RES_SHAREABLE` varchar(32) DEFAULT NULL,
  `RES_LOCATION` varchar(256) DEFAULT NULL,
  `RES_SIZE` varchar(64) DEFAULT NULL,
  `RES_SUFFIX` varchar(32) DEFAULT NULL,
  `RES_DESCRIPTION` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of wcm_general_resource
-- ----------------------------
